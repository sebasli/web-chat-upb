// import { create, Whatsapp } from 'sulla-hotfix';
const sulla = require('sulla-hotfix');

sulla.create().then(client => start(client));

/* function start(client) {
  client.onMessage(message => {
    if (message.body === 'Hi') {
      client.sendText(message.from, '👋 Hello from sulla!');
    }
  });
} */

//import { ev } from 'sulla-hotfix';
const fs1 = require('fs');

sulla.ev.on('qr', async qrcode => {
  //qrcode is base64 encoded qr code image
  //now you can do whatever you want with it
  const imageBuffer = Buffer.from(
    qrcode.replace('data:image/png;base64,', ''),
    'base64'
  );
  fs1.writeFileSync('qr_code.png', imageBuffer);
});