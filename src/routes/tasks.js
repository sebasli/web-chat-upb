const express = require("express");

const uuid = require("uuid");

const sha256 = require("sha256");

const router = express.Router();

const logger = require("../../config/winston");

const jwt = require("jsonwebtoken");
const secretToken = process.env.TOKENJWTAGT;
const secretTokenSup = process.env.TOKENJWTSUP;
const secretTokenAMD = process.env.TOKENJWTAMD;
const secretTokenDASH = process.env.TOKENJWTGRAFICAS;

var axios = require("axios");

function NOW() {
  var date = new Date();
  var aaaa = date.getFullYear();
  var gg = date.getDate();
  var mm = date.getMonth() + 1;
  if (gg < 10) gg = "0" + gg;
  if (mm < 10) mm = "0" + mm;
  var cur_day = aaaa + "-" + mm + "-" + gg;
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();
  if (hours < 10) hours = "0" + hours;
  if (minutes < 10) minutes = "0" + minutes;
  if (seconds < 10) seconds = "0" + seconds;
  return cur_day + " " + hours + ":" + minutes + ":" + seconds;
}
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Conexión BD Postgresql-------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
var pgp = require("pg-promise")(/*options*/);
const pass = process.env.POSTGRESQL_PASS;
const conexion = {
  //host: '127.0.0.1',
  //database: 'webchat_upb_local',
  //user: 'postgres',
  host: "172.10.7.67",
  port: 5432,
  database: "WebChat_UPB",
  user: "chat_sebastianlimas",
  password: pass
};
var db = pgp(conexion);
db.connect()
  .then(function (obj) {
    logger.info("La base de datos se conecto satisfactoriamente");
    obj.done(); // success, release connection;
  })
  .catch(function (error) {
    logger.error(error.message);
  });
global.db = db;
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Rutas WebChat &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
*/
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado de usuario en Base de Datos-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveUser", (req, res) => {
  const token = jwt.sign(
    { name: req.body.name, numerodocumento: req.body.numerodocumento },
    secretToken,
    { expiresIn: "14h" }
  );
  db.any(
    "INSERT INTO chat_user (name, tipo_documento, numero_documento, email, phone, tipo_solicitud, programas_interes, otros_programas, sede_usuario, otra_sede, commentary, fecha, conditions, canal, accountwa) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING (id)",
    [
      req.body.name,
      req.body.tipodocumento,
      req.body.numerodocumento,
      req.body.email,
      req.body.phone,
      req.body.tiposolicitud,
      req.body.proginteres,
      req.body.otroprog,
      req.body.sedeusuario,
      req.body.otrasede,
      req.body.commentary,
      NOW(),
      req.body.conditions,
      req.body.canal,
      null
    ]
  )
    .then(data => {
      res.status(200).send({ data: data, token: token });
    })
    .catch(function (error) {
      logger.error(error.message);
    });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado Mensajes Usuario - Bot ---------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveMessage", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.query(
        "INSERT INTO chat_message (id_chat_user, message, fecha, who) VALUES ($1, $2, $3, $4)",
        [req.body.idUser, req.body.message, NOW(), req.body.from]
      )
        .then(() => {
          res.status(200).send("success");
        })
        .catch(function (error) {
          logger.error(error.message);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado Mensajes Usuario - Agente ------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveMessageAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.query(
        "INSERT INTO chat_message_bot_agent (id_chat_user, id_socket, message, fecha, who) VALUES ($1, $2, $3, $4, $5)",
        [
          req.body.idUser,
          req.body.idSocket,
          req.body.message,
          NOW(),
          req.body.from
        ]
      )
        .then(() => {
          res.status(200).send("Succesful");
        })
        .catch(function (error) {
          logger.error(error.message);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de conversacion Webchat usuario - bot - agente-----------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callAllConversation", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      if (req.body.idUser) {
        db.any(
          "SELECT message, fecha, who, CONCAT(EXTRACT(HOUR FROM fecha),':', EXTRACT(MINUTE FROM fecha),':',EXTRACT(SECOND FROM fecha)) as hora FROM public.chat_message WHERE id_chat_user = $1 UNION SELECT  message, fecha, who, CONCAT(EXTRACT(HOUR FROM fecha),':',EXTRACT(MINUTE FROM fecha),':',EXTRACT(SECOND FROM fecha)) as hora FROM public.chat_message WHERE id_chat_user = $1 ORDER BY fecha ASC;",
          [req.body.idUser]
        )
          .then(data => {
            res.status(200).send(data);
          })
          .catch(function (error) {
            logger.error(error.message);
          });
      } else {
        res.send("You have to send an Id User for call the user");
      }
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Statu conversación Agente y Usuario ---------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/statusConvertation", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      if (req.body.idCon) {
        db.any("SELECT estado FROM public.chat_conversation WHERE id=$1;", [
          req.body.idCon
        ])
          .then(data => {
            res.status(200).send(data);
          })
          .catch(function (error) {
            logger.error(error.message);
          });
      } else {
        res.send("You have to send an Id User for call the user");
      }
    }
  });
});
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Rutas Agente &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
*/
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Logueo de agente en plataforma-----------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/login", (req, res) => {
  if (req.body.username) {
    db.one(
      "SELECT us.id, us.name, us.token, cg.name as rol, us.capacidad  FROM chat_agent as us JOIN chat_permission as gp ON gp.chat_agent_id = us.id JOIN chat_rol as cg ON cg.id = gp.chat_rol_id WHERE us.id=$1 AND  us.active=$3 AND password = $2",
      [req.body.username, sha256(req.body.password), true]
    )
      .then(data => {
        if (data.rol == "Agentes") {
          const token = jwt.sign(
            { name: req.body.username, token: req.body.token },
            secretToken,
            { expiresIn: "14h" }
          );
          db.any(
            `
          SELECT chat_group.name FROM chat_agent_group 
          JOIN chat_group ON chat_group.id = chat_agent_group.group_id
          WHERE chat_agent_group.agent_id = $1
          `,
            [data.id]
          )
            .then(group => {
              if (group) {
                res.status(200).send({
                  login: true,
                  token: data.token,
                  name: data.name,
                  jwtoken: token,
                  rol: data.rol,
                  capacity: data.capacidad,
                  group: group
                });
              } else {
                res.send("El usuario no tiene un grupo asignado");
              }
            })
            .catch(err => {
              logger.error(err.message);
            });
        } else if (data.rol == "Supervisor") {
          const token = jwt.sign(
            { name: req.body.username, token: req.body.token },
            secretTokenSup,
            { expiresIn: "14h" }
          );
          res.status(200).send({
            login: true,
            token: data.token,
            name: data.name,
            jwtoken: token,
            rol: data.rol,
            capacity: data.capacidad
          });
        } else if (data.rol == "Administrador") {
          const token = jwt.sign(
            { name: req.body.username, token: req.body.token },
            secretTokenAMD,
            { expiresIn: "14h" }
          );
          res.status(200).send({
            login: true,
            token: data.token,
            name: data.name,
            jwtoken: token,
            rol: data.rol,
            capacity: data.capacidad
          });
        }
      })
      .catch(function () {
        res.send({ login: false, token: "" });
        //logger.error(error.message);
      });
  } else {
    res.send("You have to send an Username");
  }
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado ID Conversación Usuario - Agente ------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveConvertation", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "INSERT INTO chat_conversation (socket_id, id_user, estado, id_agente, fecha, channel) VALUES ($1, $2, $3, $4, $5, $6) RETURNING (id)",
            [
              req.body.idSocket,
              req.body.idUser,
              req.body.estado,
              req.body.idAgent,
              NOW(),
              req.body.channel
            ]
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de historico Mensajes Bot-Usuario ------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callMessages", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          if (req.body.idUser) {
            db.any(
              "SELECT message, fecha, who, CONCAT(EXTRACT(HOUR FROM fecha),':', EXTRACT(MINUTE FROM fecha),':',EXTRACT(SECOND FROM fecha)) as hora  from chat_message WHERE id_chat_user =$1 ORDER BY fecha ASC",
              [req.body.idUser]
            )
              .then(data => {
                res.status(200).send(data);
              })
              .catch(function (error) {
                logger.error(error.message);
              });
          } else {
            res.send("You have to send an Id User for call the messages");
          }
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de datos de usuario--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callUser", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          if (req.body.idUser) {
            db.any(
              "SELECT us.id, us.name, us.email, us.phone, us.commentary, con.id as idcon, us.tipo_documento, us.numero_documento, us.tipo_solicitud, us.programas_interes, us.otros_programas, us.sede_usuario, us.otra_sede from chat_user as us JOIN chat_conversation as con ON con.id_user = us.id WHERE us.id = $1 AND con.estado = $2 ",
              [req.body.idUser, "Abierta"]
            )
              .then(data => {
                res.status(200).send(data);
              })
              .catch(function (error) {
                logger.error(error.message);
              });
          } else {
            res.send("You have to send an Id User for call the user");
          }
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de conversaciones activas agente-------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callConversations", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      jwt.verify(req.token, secretTokenSup, err => {
        if (err) {
          res.sendStatus(403);
        } else {
          db.one(
            "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
            ["3", req.body.tokenUser]
          )
            .then(() => {
              if (req.body.idAgent) {
                db.any(
                  "SELECT con.id AS conid, con.id_user AS idUser, con.socket_id AS sockId, con.estado AS estado, us.name AS name, us.email AS email, con.id_agente AS idAgent, (case when id_user > 0 then 0 else 0 end) as count, con.channel FROM chat_conversation AS con INNER JOIN public.chat_user AS us ON us.id = con.id_user WHERE id_agente =$1 AND estado=$2",
                  [req.body.idAgent, "Abierta"]
                )
                  .then(data => {
                    res.status(200).send(data);
                  })
                  .catch(function (error) { });
              } else {
                res.send("You have to send an Id User for call the user");
              }
            })
            .catch(function (err) {
              res.sendStatus(403);
            });
        }
      });
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          if (req.body.idAgent) {
            db.any(
              "SELECT con.id AS conid, con.id_user AS idUser, con.socket_id AS sockId, con.estado AS estado, us.name AS name, us.email AS email, us.phone, us.commentary, us.tipo_documento, us.numero_documento, us.tipo_solicitud, us.programas_interes, us.otros_programas, us.sede_usuario, us.otra_sede, con.id_agente AS idAgent, (case when id_user > 0 then 0 else 0 end) as count, con.channel FROM chat_conversation AS con INNER JOIN public.chat_user AS us ON us.id = con.id_user WHERE id_agente =$1 AND estado=$2",
              [req.body.idAgent, "Abierta"]
            )
              .then(data => {
                res.status(200).send(data);
              })
              .catch(function (error) {
                logger.error(error.message);
              });
          } else {
            res.send("You have to send an Id User for call the user");
          }
        })
        .catch(function (err) {
          logger.error(err);
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de conversaciones usuario - agente-------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callConversationAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      jwt.verify(req.token, secretTokenSup, err => {
        if (err) {
          res.sendStatus(403);
        } else {
          db.one(
            "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
            ["3", req.body.tokenUser]
          )
            .then(() => {
              if (req.body.idSocket) {
                db.any(
                  "SELECT con.socket_id AS socketid, ch.message AS message, ch.who AS who, CONCAT(EXTRACT(HOUR FROM ch.fecha),':', EXTRACT(MINUTE FROM ch.fecha),':',EXTRACT(SECOND FROM ch.fecha) ,' ',case when EXTRACT(HOUR FROM ch.fecha)>=12 THEN 'pm' ELSE 'am' end ) as hora, con.id as idcon FROM chat_message_bot_agent AS ch INNER JOIN public.chat_conversation AS con ON ch.id_socket = con.id WHERE socket_id = $1 AND con.id = $2",
                  [req.body.idSocket, req.body.idCons]
                )
                  .then(data => {
                    res.status(200).send(data);
                  })
                  .catch(function (error) {
                    logger.error(error.message);
                  });
              } else {
                res.send("You have to send an Id User for call the user");
              }
            })
            .catch(function () {
              res.sendStatus(403);
            });
        }
      });
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          if (req.body.idSocket) {
            db.any(
              "SELECT con.socket_id AS socketid, ch.message AS message, ch.who AS who, CONCAT(EXTRACT(HOUR FROM ch.fecha),':', EXTRACT(MINUTE FROM ch.fecha),':',EXTRACT(SECOND FROM ch.fecha) ,' ',case when EXTRACT(HOUR FROM ch.fecha)>=12 THEN 'pm' ELSE 'am' end ) as hora, con.id as idcon FROM chat_message_bot_agent AS ch INNER JOIN public.chat_conversation AS con ON ch.id_socket = con.id WHERE socket_id = $1 AND con.id = $2",
              [req.body.idSocket, req.body.idCons]
            )
              .then(data => {
                res.status(200).send(data);
              })
              .catch(function (error) {
                logger.error(error.message);
              });
          } else {
            res.send("You have to send an Id User for call the user");
          }
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Tipificación conversación-------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateConversation", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          if (req.body.idSocket) {
            db.query(
              "UPDATE chat_conversation SET estado=$1 WHERE socket_id =$2",
              [req.body.estado, req.body.idSocket]
            )
              .then(data => {
                res.status(200).send("success");
              })
              .catch(function (error) {
                logger.error(error.message);
              });
          } else {
            res.send("You have to send an Id User for call the user");
          }
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado Estatus Usuario - Bot ---------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveStateAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      jwt.verify(req.token, secretTokenSup, err => {
        if (err) {
          res.sendStatus(403);
        } else {
          db.one(
            "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
            ["3", req.body.tokenUser]
          )
            .then(() => {
              db.query(
                "INSERT INTO chat_agent_state (estado, id_agente, fecha, total_time) VALUES ($1, $2, $3, $4)",
                [req.body.state, req.body.idAgent, NOW(), req.body.totalTime]
              )
                .then(() => {
                  res.status(200).send("success");
                })
                .catch(function (error) {
                  logger.error(error.message);
                });
            })
            .catch(function () {
              res.sendStatus(403);
            });
        }
      });
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "INSERT INTO chat_agent_state (estado, id_agente, fecha, total_time) VALUES ($1, $2, $3, $4)",
            [req.body.state, req.body.idAgent, NOW(), req.body.totalTime]
          )
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

router.post("/callAllGroups", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.any(
        `
        SELECT id, name
        FROM public.chat_group;
        `
      )
        .then(data => {
          res.status(200).send(data);
        })
        .catch(() => {
          res.sendStatus(403);
        });
    }
  });
});

router.post("/callGroupsOfAgents", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.any(
        `
        SELECT cag.id, cg.name, cag.group_id
      	FROM public.chat_agent_group as cag
	      JOIN chat_group as cg on cg.id = cag.group_id
	      WHERE agent_id = $1;
        `,
        [req.body.agent_id]
      )
        .then(data => {
          res.status(200).send(data);
        })
        .catch(() => {
          res.sendStatus(403);
        });
    }
  });
});

router.post("/deleteGroupOfAgents", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.any(
        `
        DELETE FROM public.chat_agent_group
	      WHERE group_id = $1 AND agent_id = $2;
        `,
        [req.body.group_id, req.body.agent_id]
      )
        .then(() => {
          res.status(200).send('success');
        })
        .catch((err) => {
          logger.error(err.message);
          res.sendStatus(403);

        });
    }
  });
});

router.post("/insertGroupUser", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.any(
        `
        INSERT INTO public.chat_agent_group(group_id, agent_id)
	      VALUES ($1, $2);
        `,
        [req.body.group_id, req.body.agent_id]
      )
        .then(() => {
          res.status(200).send('success');
        })
        .catch((err) => {
          logger.error(err.message);
          res.sendStatus(403);

        });
    }
  });
});

/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Rutas Supervisor &&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizacion horarios de atención-------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateOfficeHours", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "UPDATE chat_horario_atencion SET dias_habiles_inicio=$1, dias_habiles_fin=$2, dias_no_habiles_inicio=$3, dias_no_habiles_fin=$4, fecha=$5 WHERE id=$6",
            [
              req.body.dhstart,
              req.body.dhend,
              req.body.dnhstart,
              req.body.dnhend,
              NOW(),
              1
            ]
          )
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado de agente en Base de Datos-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "INSERT INTO chat_agent (id, name, email, password, fecha, token, capacidad, active) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
            [
              req.body.cedula,
              req.body.name,
              req.body.email,
              sha256(req.body.password),
              NOW(),
              uuid(),
              req.body.capacity,
              true
            ]
          )
            .then(() => {
              res.status(200).send("success");
              db.query(
                "INSERT INTO chat_permission (chat_rol_id, chat_agent_id, fecha, status) VALUES ($1, $2, $3, $4)",
                ["1", req.body.cedula, NOW(), "Active"]
              ).catch(e => {
                logger.error(error.message);
              });
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de agente en Base de Datos-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/searchAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      logger.error(err);
      res.send(403).send(err);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.one(
            "SELECT id, name, email, active FROM chat_agent WHERE id =$1",
            [req.body.cedula]
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              res.status(200).send("No existe");
              logger.error(error.message);
            });
        })
        .catch(err => {
          logger.error(err);
          res.sendStatus(403).send(err);
        });
    }
  });
});

router.post("/searchSupervisor", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenAMD, err => {
    if (err) {
      logger.error(err);
      res.status(403).send(err);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["2", req.body.tokenUser]
      )
        .then(() => {
          let sql = `
          SELECT chat_agent.name, chat_agent.email, chat_agent.active
          FROM chat_agent
          INNER JOIN chat_permission
          ON chat_agent.id = chat_permission.chat_agent_id
          WHERE chat_agent.id = $1 and chat_permission.chat_rol_id = '3';
          `;
          db.one(sql, [req.body.cedula])
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              res.status(200).send("No existe");
              logger.error(error.message);
            });
        })
        .catch(err => {
          logger.error(err);
          res.status(403).send(err);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizacion de agente en Base de Datos-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "SELECT cg.id as exists FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $2 AND ca.id =$1 ",
            [req.body.cedula, "1"]
          )
            .then(resp => {
              if (resp[0].exists === 1) {
                db.query(
                  "UPDATE chat_agent SET name=$1, email=$2, password=$3, fecha=$4, active=$6 WHERE id =$5 ",
                  [
                    req.body.name,
                    req.body.email,
                    sha256(req.body.password),
                    NOW(),
                    req.body.cedula,
                    req.body.active
                  ]
                )
                  .then(() => {
                    res.status(200).send("success");
                  })
                  .catch(function (error) {
                    res.status(200).send("No existe");
                    logger.error(error.message);
                  });
              } else {
                res.status(200).send("No existe");
              }
            })
            .catch(function (error) {
              res.status(200).send("No existe");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de datos de Agente--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callAgents", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      console.log(err.message);
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "SELECT ca.id, ca.name as nameAgent, ca.capacidad, ca.token FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 AND ca.active=$2 ",
            ["1", true]
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function (err) {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de datos de Supervisor--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callSupervisores", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenAMD, err => {
    if (err) {
      logger.error(err);
      res.status(403).send(err);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["2", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            `SELECT chat_agent.id, chat_agent.name, chat_agent.email, chat_agent.active
            FROM chat_agent
            INNER JOIN chat_permission
            ON chat_agent.id = chat_permission.chat_agent_id
            WHERE chat_permission.chat_rol_id = $1`,
            ["3"]
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function (err) {
          logger.error(err);
          res.status(403).send(err);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizar capacidad usuario--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateCapAgents", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any("UPDATE public.chat_agent SET capacidad=$1 WHERE id=$2", [
            req.body.capacity,
            req.body.idAgent
          ])
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Revisión de horarios de atención-------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/checkOfficeHours", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.one("SELECT * from public.chat_horario_atencion;")
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado de agente en Base de Datos-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveTypify", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "INSERT INTO chat_typifications (codigo, valor, fecha) VALUES ($1, $2, $3) RETURNING (id)",
            [req.body.codTip, req.body.valueTip, NOW()]
          )
            .then(data => {
              res.status(200).send({ id: data[0].id, status: "sucess" });
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de datos de Agente--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callTypify", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "SELECT id, codigo as cod, valor as value FROM chat_typifications"
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de datos de Agente--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callTypifyAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "SELECT codigo as value, valor as text FROM chat_typifications"
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizar capacidad usuario--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateTypify", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "UPDATE public.chat_typifications SET codigo=$1, valor=$2 WHERE id=$3",
            [req.body.codTip, req.body.valueTip, req.body.idTip]
          )
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizar capacidad usuario--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/eraseTypify", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any("DELETE FROM public.chat_typifications WHERE id=$1", [
            req.body.idTip
          ])
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Guardado de agente en Base de Datos-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/saveShortcut", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "INSERT INTO chat_shortcut (shortcut, textshort, fecha) VALUES ($1, $2, $3) RETURNING (id)",
            [req.body.codTip, req.body.valueTip, NOW()]
          )
            .then(data => {
              res.status(200).send({ id: data[0].id, status: "success" });
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de datos de Agente--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callShortcut", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "SELECT id, shortcut as cod, textshort as value FROM chat_shortcut"
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Llamado de datos de Agente--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callShortcutAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["1", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "SELECT shortcut as value, textshort as text FROM chat_shortcut"
          )
            .then(data => {
              res.status(200).send(data);
            })
            .catch(function (error) {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizar capacidad usuario--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateShortcut", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "UPDATE public.chat_shortcut SET shortcut=$1, textshort=$2 WHERE id=$3",
            [req.body.codTip, req.body.valueTip, req.body.idTip]
          )
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizar capacidad usuario--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/eraseShortcut", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any("DELETE FROM public.chat_shortcut WHERE id=$1", [
            req.body.idTip
          ])
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Consulta si es día habil ---------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/isDayHabil", (req, res) => {
  if (req.body.fecha) {
    db.any(
      "SELECT cal.es_habil, cal.dia_semana, (case when cal.es_habil=true then hor.dias_habiles_inicio else hor.dias_no_habiles_inicio end) as hourStart, (case when cal.es_habil=true then hor.dias_habiles_fin else hor.dias_no_habiles_fin end) as hourEnd FROM public.chat_calendario as cal, public.chat_horario_atencion as hor WHERE cal.dia=$1;",
      [req.body.fecha]
    )
      .then(data => {
        res.status(200).send(data);
      })
      .catch(function (error) {
        logger.error(error.message);
      });
  } else {
    res.send("You have to send a date for call if is a day normal");
  }
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Consulta TMO ---------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/tmo", (req, res) => {
  db.any("SELECT * FROM public.chat_tmo")
    .then(data => {
      res.status(200).send(data);
    })
    .catch(function (error) {
      logger.error(error.message);
    });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizar TMO--------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateTmo", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.any(
            "UPDATE public.chat_tmo SET warning=$2, ending=$3 WHERE id=$1",
            ["1", req.body.warning, req.body.ending]
          )
            .then(() => {
              res.status(200).send("success");
            })
            .catch(function (error) {
              res.status(200).send("Error");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Consulta si es día habil ---------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/isDayHabil", (req, res) => {
  if (req.body.fecha) {
    db.any(
      "SELECT cal.es_habil, cal.dia_semana, (case when cal.es_habil=true then hor.dias_habiles_inicio else hor.dias_no_habiles_inicio end) as hourStart, (case when cal.es_habil=true then hor.dias_habiles_fin else hor.dias_no_habiles_fin end) as hourEnd FROM public.chat_calendario as cal, public.chat_horario_atencion as hor WHERE cal.dia=$1;",
      [req.body.fecha]
    )
      .then(data => {
        res.status(200).send(data);
      })
      .catch(function (error) {
        logger.error(error.message);
      });
  } else {
    res.send("You have to send a date for call if is a day normal");
  }
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Chequeo de que es agente-------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/checkAgent", ensureToken, (req, res) => {
  jwt.verify(req.token, secretToken, err => {
    if (err) {
      res.status(200).send("Fail");
    } else {
      res.status(200).send("success");
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Chequeo de que es supervisor-------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/checkSup", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenSup, err => {
    if (err) {
      res.status(200).send("Fail");
    } else {
      res.status(200).send("success");
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Chequeo de que es administrador-------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/checkAdministrador", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenAMD, err => {
    if (err) {
      res.status(200).send("Fail");
    } else {
      res.status(200).send("success");
    }
  });
});

/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Rutas Administrador &&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */
router.post("/saveSupervisor", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenAMD, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["2", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "INSERT INTO chat_agent (id, name, email, password, fecha, token, capacidad, active) VALUES ($1, $2, $3, $4, $5, $6, $7, true)",
            [
              req.body.cedula,
              req.body.name,
              req.body.email,
              sha256(req.body.password),
              NOW(),
              uuid(),
              0
            ]
          )
            .then(() => {
              res.status(200).send("success");
              db.query(
                "INSERT INTO chat_permission (chat_rol_id, chat_agent_id, fecha, status) VALUES ($1, $2, $3, $4)",
                ["3", req.body.cedula, NOW(), "Active"]
              ).catch(e => {
                logger.error(e);
                res.status(500).send(e);
              });
            })
            .catch(function (error) {
              logger.error(error.message);
              res.status(500).send(error);
            });
        })
        .catch(function (e) {
          logger.error(e);
          res.status(500).send(e);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Actualizacion de agente en Base de Datos-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/updateSupervisor", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenAMD, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["2", req.body.tokenUser]
      )
        .then(() => {
          db.query(
            "SELECT cg.id as exists FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $2 AND ca.id =$1 ",
            [req.body.cedula, "3"]
          )
            .then(resp => {
              if (resp[0].exists === 3) {
                // Valida si recibe contraseña para ser actualizada
                if (req.body.password) {
                  db.query(
                    "UPDATE chat_agent SET name=$1, email=$2, password=$3, fecha=$4, active=$5 WHERE id =$6",
                    [
                      req.body.name,
                      req.body.email,
                      sha256(req.body.password),
                      NOW(),
                      req.body.status,
                      req.body.cedula
                    ]
                  )
                    .then(() => {
                      res.status(200).send("success");
                    })
                    .catch(function (error) {
                      logger.error(error.message);
                      res.status(500).send(error);
                    });
                } else {
                  db.query(
                    "UPDATE chat_agent SET name=$1, email=$2, fecha=$3, active=$4 WHERE id =$5 ",
                    [
                      req.body.name,
                      req.body.email,
                      NOW(),
                      req.body.status,
                      req.body.cedula
                    ]
                  )
                    .then(() => {
                      res.status(200).send("success");
                    })
                    .catch(function (error) {
                      logger.error(error.message);
                      res.status(500).send(error);
                    });
                }
              } else {
                res.status(200).send("No existe");
              }
            })
            .catch(function (error) {
              res.status(200).send("No existe");
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Bot Analitica conversaciones agente-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/botAnalytics", ensureToken, (req, res) => {
  jwt.verify(req.token, secretTokenDASH, err => {
    if (err) {
      res.sendStatus(403);
    } else {
      db.one(
        "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
        ["3", req.body.tokenUser]
      )
        .then(() => {
          db.task(async t => {
            const conAgent = await t.any(
              "SELECT * from conversacion_por_agente where fecha BETWEEN $1 And $2 ORDER BY fecha",
              [req.body.fechainicial, req.body.fechafinal]
            );
            const conByDay = await t.any(
              "SELECT cantidad, date as fecha from conversacion_por_dia where date BETWEEN $1 And $2 ORDER BY date",
              [req.body.fechainicial, req.body.fechafinal]
            );
            const stateCon = await t.any(
              "SELECT * from estados_cantidad where mes=$1 and anio=$2",
              [req.body.month, "2019"]
            );
            const dateUse = await t.any(
              "SELECT * from fecha_mas_uso where fecha BETWEEN $1 And $2 ORDER BY fecha",
              [req.body.fechainicial, req.body.fechafinal]
            );
            const hourUse = await t.any("SELECT * from hora_mas_uso");
            const numCon = await t.any(
              "SELECT * from numero_conversaciones where fecha BETWEEN $1 And $2",
              [req.body.fechainicial, req.body.fechafinal]
            );
            const userByDay = await t.any(
              "SELECT * from usuarios_registrados_por_dia where fecha BETWEEN $1 And $2 ORDER BY fecha",
              [req.body.fechainicial, req.body.fechafinal]
            );
            return {
              conAgent,
              conByDay,
              stateCon,
              dateUse,
              hourUse,
              numCon,
              userByDay
            };
          })
            .then(data => {
              res.status(200).send(data);
            })
            .catch(error => {
              logger.error(error.message);
            });
        })
        .catch(function () {
          res.sendStatus(403);
        });
    }
  });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Relogueo de Agente -----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/reloadAgent", (req, res) => {
  const token = jwt.sign(req.body, secretTokenDASH);
  res.json({
    token
  });
});

/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Rutas Whatsapp &&&&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& */

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Consulta si es día habil ---------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/callUserWhatsapp", (req, res) => {
  db.one(
    "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1",
    ["2"]
  )
    .then(() => {
      db.one("SELECT id from public.chat_user WHERE accountwa=$1;", [
        req.body.idUser
      ])
        .then(data => {
          res.status(200).send(data);
        })
        .catch(function (error) {
          logger.error(error.message);
          res.status(200).send("");
        });
    })
    .catch(function () {
      res.sendStatus(403);
    });
});
/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Reload-----------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/reloadUserWa", (req, res) => {
  db.one(
    "SELECT cg.name nombre_grupo FROM chat_rol cg INNER JOIN chat_permission cp ON cg.id = cp.chat_rol_id INNER JOIN chat_agent ca ON cp.chat_agent_id = ca.id WHERE cg.id = $1 and ca.token=$2",
    ["2", req.body.tokenUser]
  )
    .then(() => {
      const token = jwt.sign(
        { name: req.body.name, id: req.body.id },
        secretToken,
        { expiresIn: "14h" }
      );
      res.status(200).send({ token: token });
    })
    .catch(function () {
      res.sendStatus(403);
    });
});

/*-------------------------------------------------------------------------------------------------------------------------
----------------------------------------Consulta si es día habil ---------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
router.post("/vicidial", (req, res) => {
  axios
    .post("http://190.144.246.137/integracionVicidial/insertar_upb.php", {
      name: req.body.name,
      telefono: req.body.telefono,
      email: req.body.email,
      sede: req.body.sede,
      commentary: req.body.commentary,
      id: req.body.id,
      tipId: req.body.tipId,
      tipoSol: req.body.tipoSol
    })
    .then(() => {
      res.status(200).send("succes");
    })
    .catch(function (error) {
      logger.error(error.message);
    });
});
/*
  Asegurar que exista el token
*/
function ensureToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(403);
  }
}
//Exportación de rutas
module.exports = router;
