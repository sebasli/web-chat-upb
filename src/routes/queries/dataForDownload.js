const logger = require('../../../config/winston');
//Funcion que consulta personas que ingresan al chat
async function getDataForDownload(fechaInicio, fechaFin) {
  return await global.db
    .any(
      `
      SELECT chat_agent.name AS "NombreAgente" , chat_user.name AS "NombreUsuario" , chat_user.email AS "CorreoUsuario" , chat_user.phone AS "TelefonoUsuario" , chat_user.tipo_documento AS "TipoDocumento" , chat_user.numero_documento AS "NumeroDocumento" , chat_user.fecha AS "FechaIngreso" , chat_user.fecha AS "FechaIngreso" , chat_user.programas_interes AS "ProgramasDeInteres" , chat_user.tipo_solicitud AS "TipoDeSolicitud" , chat_user.sede_usuario AS "Sede" , chat_user.canal AS "Canal" , chat_user.sede_usuario AS "Sede" , chat_user.commentary AS "Comentario"
      FROM chat_conversation 
      INNER JOIN chat_agent on chat_agent.id = chat_conversation.id_agente
      INNER JOIN chat_user on chat_user.id = chat_conversation.id_user
      WHERE chat_conversation.fecha >= date($1)  and chat_conversation.fecha <= date($2) + INTERVAL '1 day'
      `,
      [fechaInicio, fechaFin]
    )
    .then(data => {
      return data;
    })
    .catch(function(error) {
      return error;
    });
}
//Funcion que consulta tiempos del asesor
async function getDataDownload(fechaInicio, fechaFin) {
  return await global.db
    .any(
      `
      SELECT chat_agent.name AS "NombreAgente", chat_user.name AS "NombreUsuario" , chat_message_bot_agent.who AS "EmitodoPor" , 
	    chat_message_bot_agent.message AS "Chat", chat_message_bot_agent.fecha, chat_typifications.valor AS "Tipificacion"
      FROM chat_conversation
      INNER JOIN chat_agent ON chat_agent.id = chat_conversation.id_agente
      INNER JOIN chat_user ON chat_user.id = chat_conversation.id_user
      INNER JOIN chat_message_bot_agent ON chat_message_bot_agent.id_chat_user = chat_user.id
      INNER JOIN chat_typifications ON chat_typifications.codigo = chat_conversation.estado
      WHERE chat_conversation.fecha >= date($1) and chat_conversation.fecha <= date($2) + INTERVAL '1 day'
      ORDER BY chat_message_bot_agent.id_chat_user ASC, chat_message_bot_agent.fecha ASC
      `,
      [fechaInicio, fechaFin]
    )
    .then(data => {
      return data;
    })
    .catch(function(error) {
      return error;
    });
}
//Funcion que consulta bot
async function getDataBotDownload(fechaInicio, fechaFin) {
  return await global.db
    .any(
      `
      SELECT 
      chat_user.name AS "NOMBRE DEL USUARIO", 
      chat_message.who AS "ENVIADO POR", 
      regexp_replace(chat_message.message, '\r|\n', ' ', 'g') AS "MENSAJE", 
      chat_message.fecha AS "FECHA", 
      chat_user.canal AS "CANAL"
      FROM chat_message
      INNER JOIN chat_user on chat_message.id_chat_user = chat_user.id
      WHERE chat_message.fecha >= date($1) and chat_message.fecha <= date($2) + INTERVAL '1 day'
      `,
      [fechaInicio, fechaFin]
    )
    .then(data => {
      return data;
    })
    .catch(function(error) {
      return error;
    });
}


module.exports = {
  getDataForDownload,
  getDataDownload,
  getDataBotDownload
};
