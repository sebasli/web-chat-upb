//call framework express
var express = require("express");
var app = express();
//var path = require("path");
var server = require("http").Server(app);
//variable para manejar eventos de socketio, se determina el path por el cual va generar el socket
const io = require("socket.io")(server, { path: "/widget/upb/socket.io" });
//configura la variable dotenv para llamar valores del .env
const dotenv = require("dotenv");
dotenv.config();
//libreria para formatear Strings
const format = require("string-format");
//inicializa el metodo format de la libreria string-format
format.extend(String.prototype, {});
//requiere la dependencia Cors de Cross Origin Resource Sharing
const cors = require("cors");
//middleware que escucha los eventos post, get al servidor
const morgan = require("morgan");
//llamado de la configuración wiston para loggin en produccion
const logger = require("../config/winston");
/*
  Se define que va usar el servidor expres: CROS, MORGAN con WINSTON, STATIC FILES, RUTA de TASKS (API)
*/

//-------------------SULLLA--------------------
const bodyParser = require("body-parser");
const fs = require("fs-extra");
var clientSulla;
var userWhatsapp = [];
app.use(bodyParser.urlencoded({ extended: true }));
const sulla = require("sulla-hotfix");
const fs1 = require("fs");
const ev = require("sulla-hotfix").ev;
//-------------------SULLLA--------------------

app.use(cors());
//Definición de rutas staticas
app.use("/widget/upb/", express.static("src/public"));
app.use("/widget/upb/", express.static("node_modules"));
app.use("/widget/upb/", express.static("build"));
app.use("/bothuman/upb", express.static("dist"));
app.use(morgan("combined", { stream: logger.stream }));
app.use(express.json());
app.use("/widget/upb/tasks", require("./routes/tasks"));

//arrays para guardado dinamico de estado agentes, chat pendientes y conexiones
var connections = [];
var chatsPendientes = [];
var chatsFinalizados = 0;
var allMsg = 0;
var agentOnline = [];
var supervisorOnline = [];
var administradorOnline = [];
//Timer que controla el tiempo de agentes en los diferentes estados
var { Timer } = require("easytimer.js");
//Axios para solicitud post de llamado de horarios de atención en campaña
var axios = require("axios");
//Dependencia para Programador de tareas
var schedule = require("node-schedule");
var timeStart = undefined;
var timeStop = undefined;
var warning = undefined;
var ending = undefined;

var tmo = [];

const projectId = process.env.DIALOGFLOW_PROJECT_ID;

const pathUrlTask = process.env.PATH_URL_SERVER;

const tokenAmd = process.env.TOKEN_AMD;

const languageCode = "en-Es";

tmoActual();
/*
  Tarea para definir horario de atención del día, se ejecuta a las 00:01 am de la mañana
*/
var j = schedule.scheduleJob("1 0 * * *", function() {
    chatsPendientes = [];
    userWhatsapp = [];
    tmo = [];
    chatsFinalizados = 0;
    allMsg = 0;
    isHabilDay().then(data => {
        timeStart = data.hourstart;
        timeStop = data.hourend;
        logger.info(
            "Horario de atención del día de hoy inicia;" +
            timeStart +
            " finalzia: " +
            timeStop
        );
    });
});

setInterval(function() {
    io.sockets.emit("cola_chats", {
        chatP: chatsPendientes.length,
        chatF: chatsFinalizados,
        agents: agentOnline,
        allMsg: allMsg
    });
}, 1000);

io.sockets.on("connection", function(socket) {
    //Instancia de Timer para tiempo Online y Break
    var timerOnline = new Timer();
    var timerBreak = new Timer();
    var timeChat = new Timer();
    var timeChatFin = new Timer();


    //Descarga de datos que ingresan a la conversación
    socket.on("download-csv", async data => {
        const makeCSVFile = require("./utils/makeCSVFile");
        const dataForDownload = require("./routes/queries/dataForDownload");
        const arrayOfData = await dataForDownload.getDataForDownload(
            data.startDate,
            data.endDate
        );
        const fileName = makeCSVFile(arrayOfData);
        socket.emit("download-csv", fileName);
    });
    //Descarga de tiempos de conversación
    socket.on("descargar-csv", async data => {
        const makeCSVFile = require("./utils/makeCSVFile");
        const dataForDownload = require("./routes/queries/dataForDownload");
        const arrayOfData = await dataForDownload.getDataDownload(
            data.startDate,
            data.endDate
        );
        const fileName = makeCSVFile(arrayOfData);
        socket.emit("descargar-csv", fileName);
    });
    //Descarga datos de la conversacion con el bot
    socket.on("archivosbot-csv", async data => {
        const makeCSVFile = require("./utils/makeCSVFile");
        const dataForDownload = require("./routes/queries/dataForDownload");
        const arrayOfData = await dataForDownload.getDataBotDownload(
            data.startDate,
            data.endDate
        );
        const fileName = makeCSVFile(arrayOfData);
        socket.emit("archivosbot-csv", fileName);
    });


    connections.push(socket.id);
    //Escucha desconexion de usuario
    socket.on("disconnect", function() {
        try {
            let pos = searchIndexAgentSocket(socket.id);
            if (agentOnline[pos]) {
                agentOnline[pos].connect = false;
            }
        } catch (e) {
            logger.error(
                "Error, no existe propiedad connect en array de objetos agentOnline: " +
                e
            );
        }
        timerOnline.stop();
        timerBreak.stop();
        timeChat.stop();
        timeChatFin.stop();
        connections.splice(connections.indexOf(socket.id), 1);
    });
    /*
         Para manejo de Administrador
         */
    socket.on("Administrador", function(room, payload) {
        if (payload.idSocketAgent) {
            socket.join(room);
            var result = administradorOnline.find(
                soc => soc.idAgent === payload.idSocketAgent
            );
            if (result === undefined) {
                administradorOnline.push({
                    idAgent: payload.idSocketAgent,
                    numCon: payload.numeroCon,
                    sockId: socket.id,
                    state: payload.stateAgent,
                    connect: true,
                    nameAgent: payload.nameAgent,
                    capacity: payload.capacity
                });
            }
        }
    });
    /*
       Para manejo de tiempo supervisor
       */
    socket.on("Supervisor", function(room, payload) {
        if (payload.idSocketAgent) {
            socket.join(room);
            var result = supervisorOnline.find(
                soc => soc.idAgent === payload.idSocketAgent
            );
            if (result === undefined) {
                supervisorOnline.push({
                    idAgent: payload.idSocketAgent,
                    numCon: payload.numeroCon,
                    sockId: socket.id,
                    state: payload.stateAgent,
                    connect: true,
                    nameAgent: payload.nameAgent,
                    capacity: payload.capacity
                });
                let pos = searchIndexSuperv(payload.idSocketAgent);
                if (
                    payload.stateAgent === "ONLINE" ||
                    payload.stateAgent === "OCUPADO"
                ) {
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    timerOnline.start();
                } else if (payload.stateAgent === "BREAK") {
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    timerBreak.start();
                }
            } else if (result.state === "ONLINE") {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", supervisorOnline[pos].timerBreak);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "OCUPADO") {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", supervisorOnline[pos].timerBreak);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "BREAK") {
                try {
                    let pos = searchIndexSuperv(payload.idSocketAgent);
                    supervisorOnline[pos].sockId = socket.id;
                    supervisorOnline[pos].connect = true;
                    addEventimerOnlineSup(timerOnline, socket, pos);
                    addEvenTimeBreakSup(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(supervisorOnline[pos].timerBreak);
                    precisionBreak(timerBreak, valueinSeconds);
                    socket.emit("timer_online", supervisorOnline[pos].timerOnline);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerBreak en array de objetos agentOnline: " +
                        e
                    );
                }
            }
            //Cada vez que el usuario cambia a estado ONLINE
        } else if (payload.checkCola) {
            socket.join(room);
            let pos = searchIndexSuperv(room);
            try {
                if (payload.newStat) {
                    if (supervisorOnline[pos].timerOnline) {
                        var valueinSeconds = convertToInt(
                            supervisorOnline[pos].timerOnline
                        );
                        precisionOnline(timerOnline, valueinSeconds);
                    } else {
                        addEventimerOnlineSup(timerOnline, socket, pos);
                        timerOnline.start();
                    }
                    timerBreak.stop();
                    supervisorOnline[pos].state = payload.newStat;
                }
            } catch (e) {
                logger.error(
                    "Error, no existe propiedad State en array de objetos agentOnline: " +
                    e
                );
            }
            //Cada vez que el usuario cambia de estado
        } else if (payload.newState) {
            let pos = searchIndexSuperv(room);
            if (payload.newState != "OFFLINE") {
                try {
                    supervisorOnline[pos].connect = true;
                    supervisorOnline[pos].state = payload.newState;
                    if (payload.newState != "ONLINE") {
                        if (payload.newState == "OCUPADO") {
                            if (supervisorOnline[pos].timerOnline) {
                                var valueinSeconds = convertToInt(
                                    supervisorOnline[pos].timerOnline
                                );
                                timerBreak.stop();
                                precisionOnline(timerOnline, valueinSeconds);
                            } else {
                                addEventimerOnlineSup(timerOnline, socket, pos);
                                timerBreak.stop();
                                timerOnline.start();
                            }
                        } else if (supervisorOnline[pos].timerBreak) {
                            var valueinSeconds = convertToInt(
                                supervisorOnline[pos].timerBreak
                            );
                            timerOnline.stop();
                            precisionBreak(timerBreak, valueinSeconds);
                        } else {
                            addEvenTimeBreakSup(timerBreak, socket, pos);
                            timerOnline.stop();
                            timerBreak.start();
                        }
                    }
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad del array de objetos agentOnline: " + e
                    );
                }
            } else {
                supervisorOnline.splice(pos, 1);
                timerOnline.stop();
                timerBreak.stop();
            }
            //Cada vez que ingresa un nuevo usuario
        }
    });
    //Socket para Asignamiento de conversaciones a los Agentes
    socket.on("Agent", function(room, payload) {
        //cada vez que el usuario se conecta o que reinicia la página
        if (payload.idSocketAgent) {
            socket.join(room);
            var result = agentOnline.find(
                soc => soc.idAgent === payload.idSocketAgent
            );
            if (result === undefined) {
                agentOnline.push({
                    idAgent: payload.idSocketAgent,
                    numCon: payload.numeroCon,
                    sockId: socket.id,
                    state: payload.stateAgent,
                    connect: true,
                    nameAgent: payload.nameAgent,
                    capacity: payload.capacity,
                    idAg: payload.id
                });
                let pos = searchIndexAgent(payload.idSocketAgent);
                if (
                    payload.stateAgent === "ONLINE" ||
                    payload.stateAgent === "OCUPADO"
                ) {
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    timerOnline.start();
                } else if (payload.stateAgent === "BREAK") {
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    timerBreak.start();
                }
            } else if (result.state === "ONLINE") {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", agentOnline[pos].timerBreak);
                    checkChatsCola(room, pos);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "OCUPADO") {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                    precisionOnline(timerOnline, valueinSeconds);
                    socket.emit("timer_break", agentOnline[pos].timerBreak);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerOnline en array de objetos agentOnline: " +
                        e
                    );
                }
            } else if (result.state === "BREAK") {
                try {
                    let pos = searchIndexAgent(payload.idSocketAgent);
                    agentOnline[pos].sockId = socket.id;
                    agentOnline[pos].connect = true;
                    addEventimerOnline(timerOnline, socket, pos);
                    addEvenTimeBreak(timerBreak, socket, pos);
                    var valueinSeconds = convertToInt(agentOnline[pos].timerBreak);
                    precisionBreak(timerBreak, valueinSeconds);
                    socket.emit("timer_online", agentOnline[pos].timerOnline);
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad timerBreak en array de objetos agentOnline: " +
                        e
                    );
                }
            }
            //Cada vez que el usuario cambia a estado ONLINE
        } else if (payload.checkCola) {
            socket.join(room);
            let pos = searchIndexAgent(room);
            try {
                if (payload.newStat) {
                    if (agentOnline[pos].timerOnline) {
                        var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                        precisionOnline(timerOnline, valueinSeconds);
                    } else {
                        addEventimerOnline(timerOnline, socket, pos);
                        timerOnline.start();
                    }
                    timerBreak.stop();
                    agentOnline[pos].state = payload.newStat;
                }
                checkChatsCola(room, pos);
            } catch (e) {
                logger.error(
                    "Error, no existe propiedad State en array de objetos agentOnline: " +
                    e
                );
            }
            //Cada vez que el usuario cambia de estado
        } else if (payload.newState) {
            let pos = searchIndexAgent(room);
            if (payload.newState != "OFFLINE") {
                try {
                    agentOnline[pos].connect = true;
                    agentOnline[pos].state = payload.newState;
                    if (payload.newState != "ONLINE") {
                        if (payload.newState == "OCUPADO") {
                            if (agentOnline[pos].timerOnline) {
                                var valueinSeconds = convertToInt(agentOnline[pos].timerOnline);
                                timerBreak.stop();
                                precisionOnline(timerOnline, valueinSeconds);
                            } else {
                                addEventimerOnline(timerOnline, socket, pos);
                                timerBreak.stop();
                                timerOnline.start();
                            }
                        } else if (agentOnline[pos].timerBreak) {
                            var valueinSeconds = convertToInt(agentOnline[pos].timerBreak);
                            timerOnline.stop();
                            precisionBreak(timerBreak, valueinSeconds);
                        } else {
                            addEvenTimeBreak(timerBreak, socket, pos);
                            timerOnline.stop();
                            timerBreak.start();
                        }
                    }
                } catch (e) {
                    logger.error(
                        "Error, no existe propiedad del array de objetos agentOnline: " + e
                    );
                }
            } else {
                agentOnline.splice(pos, 1);
                timerOnline.stop();
                timerBreak.stop();
            }
            //Cada vez que ingresa un nuevo usuario
        } else if (payload.id) {
            if (timeStart) {
                asignAgent(socket, payload);
            } else {
                isHabilDay().then(data => {
                    timeStart = data.hourstart;
                    timeStop = data.hourend;
                    logger.info(
                        "Horario de atención del día de hoy inicia;" +
                        timeStart +
                        " finalzia: " +
                        timeStop
                    );
                    asignAgent(socket, payload);
                });
            }
        }
    });
    //Canal privado establecido entre Agente y Usuario
    socket.on("Agent-User", function(room, msg, stc, idcon, channel, who, pathFile) {
        //suscripción al canal
        socket.join(room);
        /* Reinicar bot cuando esta esperando agente */
        if (msg && msg.toLowerCase() === "hablar con monty bot") {
            let pos = searchIndexChatPen(room);
            if (pos !== -1) {
                socket.emit("new-message", {
                    msg: "Hola nuevamente, escribe menú para continuar",
                    action: "resetChat"
                });
            }
            chatsPendientes.splice(pos, 1);
        }
        /*TMO*/

        if (who === "tmoReset") {
            timeChat.reset();
            timeChatFin.stop();
        } else if (who === "tmo") {
            var resultTmo = tmo.find(soc => soc.id === room);
            if (resultTmo === undefined) {
                tmo.push({ id: room });
            }
            try {
                timeChat.start({ countdown: true, startValues: { seconds: warning } });
                let pos = searchIndexTmo(room);
                addEventimeChat(
                    timeChat,
                    timeChatFin,
                    socket,
                    pos,
                    room,
                    channel,
                    idcon
                );
            } catch (e) {
                logger.error("Error: 2 " + e);
            }
        } else if (who == "tmoFin") {
            timeChat.stop();
            timeChatFin.stop();
            timeChat = new Timer();
            timeChatFin = new Timer();
        }

        if (who === "user" && msg == null) {
            var resultTmo = tmo.find(soc => soc.id === room);
            if (resultTmo !== undefined) {
                let pos = searchIndexTmo(room);
                var valueinSeconds = convertToInt(tmo[pos].time);
                precisionTmo(timeChat, valueinSeconds);
                addEventimeChat(
                    timeChat,
                    timeChatFin,
                    socket,
                    pos,
                    room,
                    channel,
                    idcon
                );
            }
        }
        /*TMO*/

        if (stc == null || stc == 2) {
            if (msg != null) {
                allMsg += 1;
            }
            if (channel === "webchat") {

                if (!pathFile) {
                    socket.broadcast.to(room).emit("private-message", {
                        socketIdUser: room,
                        messageUser: msg,
                        state: stc,
                        idCon: idcon,
                        who: who
                    });
                } else {
                    if (msg) {
                        if (msg.includes("base64")) {
                            let tempMsg = msg.split(",")[1];
                            const imageBuffer = Buffer.from(tempMsg, "base64");
                            fs.writeFileSync(
                                `src/public/sended-files/${pathFile}`,
                                imageBuffer
                            );
                            socket.broadcast.to(room).emit("private-message", {
                                socketIdUser: room,
                                messageUser: `http://localhost:5000/widget/upb/sended-files/${pathFile}`,
                                state: stc,
                                idCon: idcon,
                                who: who
                            });
                        } else {
                            socket.broadcast.to(room).emit("private-message", {
                                socketIdUser: room,
                                messageUser: msg,
                                state: stc,
                                idCon: idcon,
                                who: who
                            });
                        }

                    }
                }

            } else if (channel === "whatsapp" && stc === 2) {
                let pos = searchIndexWhatsapp(room);
                userWhatsapp[pos].idCon = idcon;
                /*TMO*/
                var resultTmo = tmo.find(soc => soc.id === room);
                if (resultTmo === undefined) {
                    tmo.push({ id: userWhatsapp[pos].idSocket });
                }
                try {
                    userWhatsapp[pos].timeChat.start({
                        countdown: true,
                        startValues: { seconds: warning }
                    });
                    let posTmo = searchIndexTmo(userWhatsapp[pos].idSocket);
                    addEventimeChat(
                        userWhatsapp[pos].timeChat,
                        userWhatsapp[pos].timeChatFin,
                        null,
                        posTmo,
                        userWhatsapp[pos].idSocket,
                        "whatsapp",
                        userWhatsapp[pos].idCon
                    );
                } catch (e) {
                    logger.error("Error: 3" + e);
                }
                /*TMO*/
                if (msg) {
                    axios
                        .post(
                            pathUrlTask + "/saveMessageAgent", {
                                idUser: userWhatsapp[pos].id,
                                idSocket: userWhatsapp[pos].idCon,
                                message: msg,
                                from: "agent"
                            }, {
                                headers: {
                                    "Content-Type": "application/json",
                                    authorization: "Bearer " + userWhatsapp[pos].jwt
                                }
                            }
                        )
                        .catch(e => {
                            logger.error(e);
                        });
                    start(clientSulla, room, msg);
                }
            } else if (channel === "whatsapp" && stc === null) {
                if (msg) {
                    let pos = searchIndexWhatsapp(room);
                    socket.broadcast.to(room).emit("private-message", {
                        socketIdUser: room,
                        messageUser: msg,
                        state: stc,
                        idCon: idcon,
                        who: who
                    });
                    userWhatsapp[pos].timeChat.reset();
                    userWhatsapp[pos].timeChatFin.stop();
                    start(clientSulla, room, msg);
                }
            }
        } else if (stc == 1) {
            chatsFinalizados += 1;
            if (channel === "webchat") {
                socket.broadcast.to(room).emit("private-message", {
                    socketIdUser: room,
                    messageUser: "Tu conversación con el agente a finalizado",
                    state: stc,
                    idCon: idcon
                });
            } else if (channel === "whatsapp") {
                let pos = searchIndexWhatsapp(room);
                userWhatsapp[pos].bot = false;
                userWhatsapp[pos].timeChat.stop();
                userWhatsapp[pos].timeChatFin.stop();
                userWhatsapp[pos].timeChat = new Timer();
                userWhatsapp[pos].timeChatFin = new Timer();
                start(clientSulla, room, "Tu conversación con el agente a finalizado");
            }
            let pos = searchIndexAgent(msg);
            try {
                if (agentOnline[pos].numCon > 0) {
                    agentOnline[pos].numCon -= 1;
                }
            } catch {
                logger.error(
                    "Error, no existe propiedad numCon en array de objetos agentOnline"
                );
            }
        }
    });
    //Nuevo horario
    socket.on("new_office_hour", function(data) {
        switch (data) {
            case 1:
                isHabilDay().then(resp => {
                    timeStart = resp.hourstart;
                    timeStop = resp.hourend;
                    logger.info(
                        "Nuevo Horario de atención del día de hoy inicia;" +
                        timeStart +
                        " finalzia: " +
                        timeStop
                    );
                });
                break;
        }
    });
    //Nueva Capacidad Agente
    socket.on("new_capacity", function(data) {
        try {
            let pos = searchIndexAgent(data.agent);
            if (pos != -1) {
                agentOnline[pos].capacity = data.newCap;
                io.sockets.emit("new_info", {
                    agent: data.agent,
                    capacity: data.newCap
                });
            }
        } catch (e) {
            logger.error(
                "Error, no existe propiedad numCon en array de objetos agentOnline" + e
            );
        }
    });
    //Nuevo TMO
    socket.on("new_tmo", function(data) {
        try {
            warning = data.warning * 60;
            ending = data.ending * 60;
        } catch (e) {
            logger.error(
                "Error, no existe propiedad numCon en array de objetos agentOnline" + e
            );
        }
    });
    //Mensajes entre Dialogflow y Usuario
    socket.on("new-message", async function(data) {
        var sessionId = "{0.id}".format(data);
        var message = data.message;
        const obj = await detectTextIntent(
            projectId,
            sessionId,
            message,
            languageCode
        );
        try {
            var actionQuery = obj[0].queryResult.action;

            /*Para sumar al count de widget polymer
            if(actionQuery == 'default'){
              socket.emit('estado', 'nuevo');
            }*/
            if (actionQuery == "human") {
                socket.emit("estado", "agente");
            }
            var mensajes = obj[0].queryResult.fulfillmentMessages;
            mensajes.forEach(function(e) {
                if (e.platform === "ACTIONS_ON_GOOGLE") {
                    try {
                        let checker = comprobarSuggestion(actionQuery)
                        if (checker) {
                            let messag = handlerSuggestions(actionQuery)
                            let preguntas = messag.preguntas
                            let estaAccion = messag.actionQuery
                            let msgAccion = messag.mensaje

                            socket.emit('new-message', { sug: preguntas, action: estaAccion });
                            socket.emit('new-message', { action: estaAccion, msg: msgAccion });

                        } else
                            var textToSpeech =
                                e.simpleResponses.simpleResponses[0].textToSpeech;
                        socket.emit("new-message", {
                            msg: textToSpeech,
                            action: actionQuery
                        });
                        //Suggestions Quemadas 
                        console.log(obj)
                    } catch {
                        if (obj[0].queryResult.action == "Bellas_Artes_Dswsoctorado") {
                            var servicios = [
                                { "title": "Doctorado en Estudios de Diseño" }
                            ];
                            socket.emit("new-message", {
                                sug: servicios,
                                action: actionQuery
                            });
                        } else if (obj[0].queryResult.action == "Cliente_si_cuenta_cobranza") {
                            var serviciosDos = [
                                { "title": "¿Cómo funciona el pago del cargo de conexión mediante cupón de pago?" },
                            ];
                            socket.emit("new-message", {
                                sug: serviciosDos,
                                action: actionQuery
                            });
                        }
                        //Suggestions Normales
                        else {
                            var suggestions = e.suggestions.suggestions;
                            socket.emit("new-message", {
                                sug: suggestions,
                                action: actionQuery
                            });
                        }
                    }
                } else if (e.platform === "KIK") {
                    var textToSpeech = e.text.text[0];
                    socket.emit("new-message", {
                        msg: textToSpeech,
                        action: actionQuery
                    });
                }
            });
        } catch {
            console.log("AQUI ERROR")
            logger.error("Error, no hay resultado del API de Dialogflow");
        }
    });
});
/*
  Determina el puerto por el cual se va desplegar la aplicación
*/
server.listen(5000, function() {
    logger.info("Server On en http://localhost:5000/widget/upb");
});

function comprobarSuggestion(actionQuery) {
    let a = 'Educación_y_Pedagogía_Doctorado'
    let b = 'Ciencias_de_la_Salud_Doctorado'
    let c = 'Ciencias_Sociales_Doctorado'
    let d = 'Ingenierías_Doctorado'
    let e = 'Economía_Administración_Contaduría_Afines_Maestria'
    let f = 'Ciencias_Sociales_Maestria'
    let g = 'Ciencias_de_la_Salud_Maestria'
    let h = 'Ingenierías_Maestria'
    let i = 'Educación_y_Pedagogía_Maestria'
    let j = 'Ciencias_Sociales_y_Estratégicas_Maestria'
    let k = 'Ingeniería_Ambiental_Sanitaria_Y_Afines_Maestria'
    let l = 'Ciencias_Sociales_Especializacion'
    let m = 'Ingenierías_Especializacion'
    let n = 'Bellas_Artes_Especializacion'
    let o = 'Ingeniería_Ambiental_Sanitaria_Y_Afines_Especializacion'
    let p = 'Economia_Administracion_Contaduria_y_Afines_Especializacion'
    let q = 'Ciencias_Sociales_y_Estrategicas_Especializacion'
    let r = 'Postgrado_Doctorado'
    let s = 'Postgrado_Maestria'
    let t = 'Postgrado_Licenciatura'
    let x = 'Postgrado_Especialización'
    let z = 'Ingresoala_u'
    let aa = 'Carreras_Profesionales'
    let bb = 'CP_Urbanismo'
    let cc = 'Ciencias_humanas'
    let dd = 'welcome'
    let ee = 'Procesos_Financieros'
    let ff = 'Ciencias_de_la_Salud_Especializacion'
    let gg = 'Educación_y_Pedagogía_Especializacion'
    let hh = 'Derecho_y_Ciencias_Políticas_Maestria'
    let ii = 'Derecho_y_Ciencias_Políticas_Especializacion'
    let jj = 'Derecho_y_Ciencias_Políticas_Doctorado'
    let kk = 'Teología_Filosofía_y_Humanidades_Doctorado'
    let ll = 'Teología_Filosofía_y_Humanidades_Maestria'
    let mm = 'Arquitectura_y_Diseño_Especializacion'
    let nn = 'Arquitectura_y_Diseño_Maestria'
    let oo = 'Arquitectura_y_Diseño_Doctorado'
    let pp = 'Economía_Administración_y_Negocios_Maestria'
    let qq = 'Economía_Administración_y_Negocios_Especializacion'
    let ññ = 'Edu_Peda'
    let rr = 'Economia_Admin'
    let xx = 'Ciencias_Sociales_PP'
    if (actionQuery == a || actionQuery == b || actionQuery == c || actionQuery == d || actionQuery == e || actionQuery == f || actionQuery == g || actionQuery == h || actionQuery == i ||
        actionQuery == j || actionQuery == k || actionQuery == l || actionQuery == m || actionQuery == n || actionQuery == o || actionQuery == p || actionQuery == q || actionQuery == r ||
        actionQuery == s || actionQuery == t || actionQuery == x || actionQuery == z || actionQuery == aa || actionQuery == bb || actionQuery == cc || actionQuery == dd || actionQuery == ee ||
        actionQuery == ff || actionQuery == gg || actionQuery == hh || actionQuery == ii || actionQuery == jj || actionQuery == kk || actionQuery == ll || actionQuery == mm || actionQuery == nn ||
        actionQuery == oo || actionQuery == pp || actionQuery == qq || actionQuery == ññ || actionQuery == rr || actionQuery == xx) {
        return true
    } else {
        return false
    }
}



function handlerSuggestions(actionQuery) {
    //Suggestions Postgrados
    if (actionQuery == 'Educación_y_Pedagogía_Doctorado') {
        let preguntas = [
            { "title": "Doctorado en Educación" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Educación_y_Pedagogía_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Didáctica de las Ciencias: Física y Matemáticas" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_de_la_Salud_Doctorado') {
        let preguntas = [
            { "title": "Doctorado en Ciencias Médicas" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_Sociales_Doctorado') {
        let preguntas = [
            { "title": "Doctorados en Comunicación" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ingenierías_Doctorado') {
        let preguntas = [
            { "title": "Doctorado en Gestión de la Tecnología y la Innovación" },
            { "title": "Doctorado en Ingeniería" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Economía_Administración_y_Negocios_Maestria') {
        let preguntas = [
            { "title": "Maestría en Administración" },
            { "title": "Maestría en Dirección de Marketing" },
            { "title": "Maestría en Economía de la Innovación y el Cambio Técnico" },
            { "title": "Maestría en Gerencia de Proyectos" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_Sociales_Maestria') {
        let preguntas = [
            { "title": "Maestría en Cine Documental" },
            { "title": "Maestría en Comportamiento del Consumidor" },
            { "title": "Maestría en Comunicación Digital" },
            { "title": "Maestría en Comunicación Organizacional" },
            { "title": "Maestría en Desarrollo" },
            { "title": "Maestría en Gestión Humana para Organizaciones Saludables" },
            { "title": "Maestría en Industrias Creativas y Culturales" },
            { "title": "Maestría en Psicología Social" },
            { "title": "Maestría en Psicología y Salud Mental" },
            { "title": "Maestría en Psicopedagogía" },
            { "title": "Maestría en Psicopedagogía Virtual" },
            { "title": "Maestría en Psicoterapia" },
            { "title": "Maestría en Televisión Digital" },
            { "title": "Maestría en Terapia Familiar" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_de_la_Salud_Maestria') {
        let preguntas = [
            { "title": "Maestría en Bioética y Bioderecho" },
            { "title": "Maestría en Ciencias Médicas" },
            { "title": "Maestría en Enfermería Oncológica" },
            { "title": "Maestría en Gestión Integral y Calidad en Salud" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ingenierías_Maestria') {
        let preguntas = [
            { "title": "Maestría en Ciencias Naturales y Matemática" },
            { "title": "Maestría en Diseño y Gestión de Procesos Industriales" },
            { "title": "Maestría en Gestión Tecnológica" },
            { "title": "Maestría en Ingeniería" },
            { "title": "Maestría en Innovación en Agronegocios" },
            { "title": "Maestría en Tecnologías de la Información y la Comunicación" },
            { "title": "Maestría en Sostenibilidad" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Educación_y_Pedagogía_Maestria') {
        let preguntas = [
            { "title": "Maestría en Literatura" },
            { "title": "Maestría en Procesos de Aprendizaje y Enseñanza de Segundas Lenguas" },
            { "title": "Maestría en Educación" },
            { "title": "Maestría en Literatura Virtual" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Derecho_y_Ciencias_Políticas_Maestria') {
        let preguntas = [
            { "title": "Maestría en Estudios Políticos" },
            { "title": "Maestría en Gestión de la Propiedad Intelectual" },
            { "title": "Maestría en Derecho" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Derecho_y_Ciencias_Políticas_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Derecho Urbanístico e Inmobiliario" },
            { "title": "Especialización en Legislación Tributaria" },
            { "title": "Especialización en Derecho Administrativo" },
            { "title": "Especialización en Derecho Comercial" },
            { "title": "Especialización en Derecho Médico" },
            { "title": "Especialización en Derecho Penal y Procesal Penal" },
            { "title": "Especialización en Derecho Privado" },
            { "title": "Especialización en Derecho Procesal" },
            { "title": "Especialización en Derecho de Familia, Infancia y Adolescencia" },
            { "title": "Especialización en Derecho del Trabajo y la Seguridad Social" },
            { "title": "Especialización en Legislación Financiera y del Mercado de Valores" },
            { "title": "Especialización en Responsabilidad Civil y Seguros" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Derecho_y_Ciencias_Políticas_Doctorado') {
        let preguntas = [
            { "title": "Doctorados en Estudios Políticos y Jurídicos" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ingeniería_Ambiental_Sanitaria_Y_Afines_Maestria') {
        let preguntas = [
            { "title": "Maestría en Sostenibilidad" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Teología_Filosofía_y_Humanidades_Doctorado') {
        let preguntas = [
            { "title": "Doctorados en Filosofía" },
            { "title": "Doctorados en Teología" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Teología_Filosofía_y_Humanidades_Maestria') {
        let preguntas = [
            { "title": "Maestría en Filosofía" },
            { "title": "Maestría en Teología" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Arquitectura_y_Diseño_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Proyectos de Arquitectura Interior" },
            { "title": "Especialización en Gestión Empresarial para la Arquitectura" },
            { "title": "Especialización en Gestión para la Intervención del Patrimonio Cultural" },
            { "title": "Especialización en Diseño de Mobiliario" },
            { "title": "Especialización en Diseño Estratégico e Innovación" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Arquitectura_y_Diseño_Maestria') {
        let preguntas = [
            { "title": "Maestría en Diseño del Paisaje" },
            { "title": "Maestría en Urbanismo" },
            { "title": "Maestría en Arquitectura Crítica y Proyecto" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Arquitectura_y_Diseño_Doctorado') {
        let preguntas = [
            { "title": "Doctorados en Estudios de Diseño" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ingenierías_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Procesos Textiles de Alto Valor" },
            { "title": "Especialización en Agronegocios" },
            { "title": "Especialización en Automática" },
            { "title": "Especialización en Gestión Ambiental" },
            { "title": "Especialización en Gestión de la Innovación Tecnológica" },
            { "title": "Especialización en Ingeniería Biomédica" },
            { "title": "Especialización en Inteligencia de Negocios" },
            { "title": "Especialización en Robótica y Mecatrónica" },
            { "title": "Especialización en Seguridad Informática" },
            { "title": "Especialización en Sistemas Integrados de Gestión" },
            { "title": "Especialización en Sistemas de Transmisión y Distribución de Energía Eléctrica" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_de_la_Salud_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Anestesia Cardiovascular" },
            { "title": "Especialización en Anestesiología" },
            { "title": "Especialización en Cardiología" },
            { "title": "Especialización en Cardiología Intervencionista y Hemodinámica" },
            { "title": "Especialización en Cardiología Pediátrica" },
            { "title": "Especialización en Cirugía Cardiovascular" },
            { "title": "Especialización en Cirugía General" },
            { "title": "Especialización en Cuidados Paliativos" },
            { "title": "Especialización en Dermatología" },
            { "title": "Especialización en Dolor y Cuidado Paliativo" },
            { "title": "Especialización en Ecocardiografía" },
            { "title": "Especialización en Endocrinología" },
            { "title": "Especialización en Enfermedades Infecciosas" },
            { "title": "Especialización en Enfermería Cardiovascular" },
            { "title": "Especialización en Gerencia en Economía y Finanzas de la Salud" },
            { "title": "Especialización en Ginecología y Obstetricia" },
            { "title": "Especialización en Medicina Crítica y Cuidados Intensivos" },
            { "title": "Especialización en Medicina Interna" },
            { "title": "Especialización en Medicina Materno Fetal" },
            { "title": "Especialización en Medicina de la Actividad Física y el Deporte" },
            { "title": "Especialización en Nefrología" },
            { "title": "Especialización en Neonatología" },
            { "title": "Especialización en Oftalmología" },
            { "title": "Especialización en Ortopedia y Traumatología" },
            { "title": "Especialización en Ortopedia y Traumatología Pediátrica" },
            { "title": "Especialización en Pediatría" },
            { "title": "Especialización en Psiquiatría" },
            { "title": "Especialización en Psiquiatría de Enlace" },
            { "title": "Especialización en Radiología e Imágenes Diagnósticas" },
            { "title": "Especialización en Reumatología" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Bellas_Artes_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Diseño Estratégico e Innovación" }
        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ingeniería_Ambiental_Sanitaria_Y_Afines_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Preservación y Conservación de los Recursos Naturales" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_Sociales_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Comunicación y Periodismo Digital" },
            { "title": "Especialización en Familia" },
            { "title": "Especialización en Gerencia de la Comunicación Organizacional" },
            { "title": "Especialización en Psicología Clínica y Salud Mental" },
            { "title": "Especialización en Psicología Social Aplicada" },
            { "title": "Especialización en Gestión Humana" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Economía_Administración_y_Negocios_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Gestión de Proyectos" },
            { "title": "Especialización en Economía de la Innovación y el Cambio Técnico" },
            { "title": "Especialización en Estrategia Gerencial y Prospectiva" },
            { "title": "Especialización en Gerencia" },
            { "title": "Especialización en Gerencia Financiera" },
            { "title": "Especialización en Gerencia Pública" },
            { "title": "Especialización en Gerencia de Marketing" },
            { "title": "Especialización en Gerencia de Mercadeo" },
            { "title": "Especialización en Gerencia de Proyectos" },
            { "title": "Especialización en Gerencia de Sistemas y Tecnología" },
            { "title": "Especialización en Gerencia del Talento Humano" },
            { "title": "Especialización en Gerencia para Ingenieros" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_Sociales_y_Estrategicas_Especializacion') {
        let preguntas = [
            { "title": "Especialización en Derecho Administrativo" },
            { "title": "Especialización en Derecho Comercial" },
            { "title": "Especialización en Derecho Médico" },
            { "title": "Especialización en Derecho Penal y Procesal Penal" },
            { "title": "Especialización en Derecho Privado" },
            { "title": "Especialización en Derecho Procesal" },
            { "title": "Especialización en Derecho de Familia, Infancia y Adolescencia" },
            { "title": "Especialización en Derecho del Trabajo y la Seguridad Social" },
            { "title": "Especialización en Legislación Financiera y del Mercado de Valores" },
            { "title": "Especialización en Responsabilidad Civil y Seguros" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == "Postgrado_Doctorado") {
        let preguntas = [
            { "title": "Ciencias Sociales" },
            { "title": "Ingenierías" },
            { "title": "Ciencias de la Salud" },
            { "title": "Educación y Pedagogía" },
            { "title": "Derecho y Ciencias Políticas" },
            { "title": "Teología, Filosofía y Humanidades" },
            { "title": "Arquitectura y Diseño" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Postgrado_Maestria') {
        let preguntas = [
            { "title": "Ciencias Sociales" },
            { "title": "Economía, Administración y Negocios" },
            { "title": "Ingenierías" },
            { "title": "Ciencias de la Salud" },
            { "title": "Educación y Pedagogía" },
            { "title": "Derecho y Ciencias Políticas" },
            { "title": "Teología, Filosofía y Humanidades" },
            { "title": "Arquitectura y Diseño" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Postgrado_Especialización') {
        let preguntas = [
            { "title": "Ciencias Sociales" },
            { "title": "Economía, Administración y Negocios" },
            { "title": "Ingenierías" },
            { "title": "Ciencias de la Salud" },
            { "title": "Educación y Pedagogía" },
            { "title": "Derecho y Ciencias Políticas" },
            { "title": "Arquitectura y Diseño" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    }

    //FALTANTES
    else if (actionQuery == 'Ingresoala_u') {
        let preguntas = [
            { "title": "Portafolio Académico" },
            { "title": "Becas Descuentos y Alternativas financieras" },
            { "title": "Formulario de  Inscripción" },
            { "title": "Menú" },
            

        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Técnicas_y_Tecnologías_IngresoU') {
        let preguntas = [
            { "title": "..." }
        ]
        return { preguntas, actionQuery }
    } else if (actionQuery == 'Técnicas_y_Tecnologías_Programa_Tu_Entrevista') {
        let preguntas = [
            { "title": "..." }
        ]
        return { preguntas, actionQuery }
    }
    //Suggestions Pregrados
    else if (actionQuery == 'Carreras_Profesionales') {
        let preguntas = [
            { "title": "Ingenierías" },
            { "title": "Educación y Pedagogía" },
            { "title": "Ciencias de la salud" },
            { "title": "Derecho y Ciencias Políticas" },
            { "title": "Ciencias Sociales" },
            { "title": "Arquitectura y Diseño" },
            { "title": "Economía, Administración y Negocios" },
            { "title": "Teología, Filosofía y Humanidades" },
            { "title": "Menú" }


        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'CP_Urbanismo') {
        let preguntas = [
            { "title": "Ingeniería Administrativa" },
            { "title": "Ingeniería Aeronáutica" },
            { "title": "Ingeniería Agroindustrial" },
            { "title": "Ingeniería Ambiental" },
            { "title": "Ingeniería Electrónica" },
            { "title": "Ingeniería Eléctrica" },
            { "title": "Ingeniería Industrial" },
            { "title": "Ingeniería Mecánica" },
            { "title": "Ingeniería Química" },
            { "title": "Ingeniería de Sistemas e Informática" },
            { "title": "Ingeniería en Ciencia de Datos" },
            { "title": "Ingeniería en Diseño de Entretenimiento Digital" },
            { "title": "Ingeniería en Nanotecnología" },
            { "title": "Menú" }


        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Edu_Peda') {
        let preguntas = [
            { "title": "Licenciatura en Español - Inglés (a distancia)" },
            { "title": "Licenciatura en Etnoeducación" },
            { "title": "Licenciatura en Epañol - Inglés (Presencial)" },
            { "title": "Menú" }


        ]
        let mensaje = "Elige alguna de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'welcome') {
        let preguntas = [
            { "title": "Ingreso a la Universidad" },
            { "title": "Programa tu Entrevista o Asesoría" },
            { "title": "Solicitud de Servicios" },
            { "title": "Procesos Financieros" },
            { "title": "Vive UPB" },
            { "title": "Menú" }


        ]
        let mensaje = " ¡Hola! Soy tu Asesora Virtual de la Universidad Pontificia Bolivariana. Me alegra mucho que te hayas comunicado con nosotros, cuéntame en qué te puedo ayudar?"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Procesos_Financieros') {
        let preguntas = [
            { "title": "Saldo a favor" },
            { "title": "Medios de pago" },
            { "title": "Inconvenientes con el pago" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige una de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Economia_Admin') {
        let preguntas = [
            { "title": "Administración de Empresas" },
            { "title": "Economía" },
            { "title": "Gestión  Emprendimiento y la Innovación" },
            { "title": "Negocios Internacionales" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige una de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    } else if (actionQuery == 'Ciencias_Sociales_PP') {
        let preguntas = [
            { "title": "Comunicación Social - Periodismo" },
            { "title": "Psicología" },
            { "title": "Publicidad" },
            { "title": "Trabajo Social" },
            { "title": "Menú" }

        ]
        let mensaje = "Elige una de las siguientes opciones"
        return { preguntas, actionQuery, mensaje }
    }

}
/*
 Función para detectar intentos de Dialogflow y retornar queryResult
*/
function detectTextIntent(projectId, sessionId, queries, languageCode) {
    // [START dialogflow_detect_intent_text]
    // Imports the Dialogflow library
    const dialogflow = require("dialogflow");
    // Instantiates a session client
    const sessionClient = new dialogflow.SessionsClient();
    // The path to identify the agent that owns the created intent.
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);
    //variable for save object response from Dialogflow
    let response;
    // The text query request.
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: queries,
                languageCode: languageCode
            }
        }
    };
    //detect Intent for request
    response = sessionClient.detectIntent(request);
    //return object with queryResult
    return response;
}
/*
 Función para buscar indice en array de agentes segun valor del Agent ID
*/
function searchIndexAgent(value) {
    pos = agentOnline.findIndex(ele => ele.idAgent === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes segun valor del Socket ID
*/
function searchIndexAgentSocket(value) {
    pos = agentOnline.findIndex(ele => ele.sockId === value);
    return pos;
}
/*
Función para buscar indice en array de chats pendientes segun el socketid
+*/
function searchIndexChatPen(value) {
    pos = chatsPendientes.findIndex(ele => ele.socketId === value);
    return pos;
}
/*
 Función para buscar indice en array de supervisor segun valor del Agent ID
*/
function searchIndexSuperv(value) {
    pos = supervisorOnline.findIndex(ele => ele.idAgent === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes segun valor del Agent ID
*/
function searchIndexTmo(value) {
    pos = tmo.findIndex(ele => ele.id === value);
    return pos;
}
/*
 Función para buscar indice en array de supervisor segun valor del Socket ID
*/
function searchIndexAgentSuperv(value) {
    pos = supervisorOnline.findIndex(ele => ele.sockId === value);
    return pos;
}
/*
 Función para buscar indice en array de agentes, con menos conversaciones
*/
function findMinCon(arr) {
    let min = arr[0].numCon,
        ind = 0,
        v;
    for (let i = 1, len = arr.length; i < len; i++) {
        v = arr[i].numCon;
        v < min ? ((min = v), (ind = i)) : ((min = min), ind);
    }
    return ind;
}
/*
  Funcion para convertir formato de hora-minutos-segundos hh:mm:ss a segundos
*/
function convertToInt(num) {
    try {
        let arr = num.split(":");
        let hr = parseInt(arr[0]);
        let min = parseInt(arr[1]);
        let seg = parseInt(arr[2]);
        let result = hr * 3600 + min * 60 + seg;
        return result;
    } catch {
        logger.error("Error, no existe array para hacerle split");
    }
}
/*
  Funcion para convertir segundos a formato de hora-minutos hh:mm am/pm
*/
function convertToHour(num) {
    try {
        var hours = Math.floor(num / 3600);
        var minutes = Math.floor((num % 3600) / 60);
        if (hours < 10) hours = "0" + hours;
        if (minutes < 10) minutes = "0" + minutes;
        if (hours > 12) {
            hours = hours - 12;
            apm = "pm";
        } else if (hours == 12) {
            apm = "pm";
        } else {
            apm = "am";
        }
        return hours + ":" + minutes + " " + apm;
    } catch {
        logger.error("Error, no existe array para hacerle split");
    }
}
/*
  Funcion para obtener formato de año-mes-dia yyyy:mm:dd
*/
function nowDate() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = date.getMonth() + 1;
    if (gg < 10) gg = "0" + gg;
    if (mm < 10) mm = "0" + mm;
    var cur_day = aaaa + "-" + mm + "-" + gg;
    return cur_day;
}
/*
  Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
*/
function nowHour() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds;
}
/*
  Funcion para llamada de horario de atención del día
*/
function isHabilDay() {
    return axios
        .post(pathUrlTask + "/isDayHabil", { fecha: nowDate() })
        .then(function(data) {
            return data.data[0];
        })
        .catch(e => {
            logger.error(
                "Error, no existe propiedad numCon en array de objetos agentOnline: " +
                e
            );
            return null;
        });
}
/*
  Funcion para llamada de horario de atención del día
*/
function tmoActual() {
    return axios
        .post(pathUrlTask + "/tmo", {})
        .then(function(resp) {
            warning = resp.data[0].warning * 60;
            ending = resp.data[0].ending * 60;
        })
        .catch(e => {
            logger.error("Error: 1 " + e);
        });
}
/*
  Funcion para asignamiento de agentes, según horario de atención
*/
function asignAgent(socket, payload) {
    var hour = convertToInt(nowHour());
    if (hour >= timeStart && hour <= timeStop) {
        var result = agentOnline.filter(
            soc =>
            soc.state === "ONLINE" &&
            soc.connect === true &&
            soc.numCon < soc.capacity
        );
        if (result === undefined || result.length === 0) {
            socket.emit("new-message", {
                msg: "En este momento no tenemos agentes disponibles, por favor espera un momento.",
                action: null
            });
            chatsPendientes.push(payload);
            socket.emit("messages", null);
        } else {
            try {
                let x = findMinCon(result);
                socket.broadcast
                    .to(result[x].idAgent)
                    .emit("event", payload, "webchat");
                let pos = searchIndexAgent(result[x].idAgent);
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error("Error asignando conversacion al agente: " + e);
            }
        }
    } else {
        socket.emit("new-message", {
            msg: "En este momento estamos fuera del horario de atención, podras comunicarte con un agente de " +
                convertToHour(timeStart) +
                " hasta " +
                convertToHour(timeStop) +
                ".\nSi deseas, puedes seguir hablando con el bot",
            action: "resetChat"
        });
    }
}
/*
  Función para asignar Chat en Cola al agente que finaliza conversación
*/
function checkChatsCola(room, pos) {
    let capacity = agentOnline[pos].capacity;
    let conversation = agentOnline[pos].numCon;
    let capTot = capacity - conversation;
    if (chatsPendientes.length > 0 && conversation < capacity) {
        chatsPendientes.slice(0, capTot).forEach(e => {
            io.in(room).emit("event", e, e.channel);
            try {
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error(
                    "Error, no existe propiedad numCon en array de objetos agentOnline: " +
                    e
                );
            }
        });
        chatsPendientes.splice(0, capTot);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimerOnline(timerOnline, socket, pos) {
    try {
        timerOnline.addEventListener("secondsUpdated", function(e) {
            agentOnline[pos].timerOnline = timerOnline.getTimeValues().toString();
            socket.emit("timer_online", timerOnline.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEvenTimeBreak(timerBreak, socket, pos) {
    try {
        timerBreak.addEventListener("secondsUpdated", function(e) {
            agentOnline[pos].timerBreak = timerBreak.getTimeValues().toString();
            socket.emit("timer_break", timerBreak.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimerOnlineSup(timerOnline, socket, pos) {
    try {
        timerOnline.addEventListener("secondsUpdated", function(e) {
            supervisorOnline[
                pos
            ].timerOnline = timerOnline.getTimeValues().toString();
            socket.emit("timer_online", timerOnline.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEvenTimeBreakSup(timerBreak, socket, pos) {
    try {
        timerBreak.addEventListener("secondsUpdated", function(e) {
            supervisorOnline[pos].timerBreak = timerBreak.getTimeValues().toString();
            socket.emit("timer_break", timerBreak.getTimeValues().toString());
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para iniciar el contador de tiempo Online en los segundos precisos que estaba
*/
function precisionOnline(timerOnline, valueinSeconds) {
    try {
        timerOnline.start({
            precision: "seconds",
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error("Error iniciando timer: " + e);
    }
}
/*
  Función para iniciar el contador de tiempo en Break los segundos precisos que estaba
*/
function precisionBreak(timerBreak, valueinSeconds) {
    try {
        timerBreak.start({
            precision: "seconds",
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error("Error iniciando timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimeChat(timeC, timeCFin, socket, pos, room, channel, idCon) {
    try {
        tmo[pos].time = timeC.getTimeValues().toString();
        timeC.addEventListener("secondsUpdated", function(e) {
            tmo[pos].time = timeC.getTimeValues().toString();
        });
        timeC.addEventListener("reset", function(e) {
            tmo[pos].time = timeC.getTimeValues().toString();
        });
        timeC.addEventListener("targetAchieved", function(e) {
            io.in(room).emit("private-message", {
                socketIdUser: room,
                messageUser: "Tu conversación se finalizará en " +
                    ending / 60 +
                    " minuto(s) si no respondes algo",
                state: 4,
                idCon: idCon,
                who: "agent"
            });
            timeC.stop();
            if (channel === "webchat") {
                timeCFin.start({ countdown: true, startValues: { seconds: ending } });
                let pos = searchIndexTmo(room);
                addEventimeChatFin(timeCFin, socket, pos, room, channel, idCon);
            } else if (channel === "whatsapp") {
                start(
                    clientSulla,
                    room,
                    "Tu conversación se finalizará en " +
                    ending / 60 +
                    " minuto(s) si no respondes algo"
                );
                timeCFin.start({ countdown: true, startValues: { seconds: ending } });
                let pos = searchIndexTmo(room);
                addEventimeChatFin(timeCFin, socket, pos, room, channel, idCon);
            }
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para asignar Listener a la instancia de easytimer 
*/
function addEventimeChatFin(timeCF, socket, pos, room, channel, idCon) {
    try {
        tmo[pos].timeFin = timeCF.getTimeValues().toString();
        timeCF.addEventListener("secondsUpdated", function(e) {
            tmo[pos].timeFin = timeCF.getTimeValues().toString();
        });
        timeCF.addEventListener("reset", function(e) {
            tmo[pos].timeFin = timeCF.getTimeValues().toString();
        });
        timeCF.addEventListener("targetAchieved", function(e) {
            if (channel === "webchat") {
                io.in(room).emit("private-message", {
                    socketIdUser: room,
                    messageUser: null,
                    state: 5,
                    idCon: idCon,
                    who: "agent",
                    channel: channel
                });
            } else if (channel === "whatsapp") {
                io.in(room).emit("private-message", {
                    socketIdUser: room,
                    messageUser: null,
                    state: 5,
                    idCon: idCon,
                    who: "agent",
                    channel: channel
                });
                let posWha = searchIndexWhatsapp(room);
                userWhatsapp[posWha].bot = false;
                userWhatsapp[posWha].timeChat.stop();
                userWhatsapp[posWha].timeChatFin.stop();
                userWhatsapp[posWha].timeChat = new Timer();
                userWhatsapp[posWha].timeChatFin = new Timer();
            }
        });
    } catch (e) {
        logger.error("Error añaniendo evento timer: " + e);
    }
}
/*
  Función para iniciar el contador de tiempo en chat los segundos precisos que estaba
*/
function precisionTmo(timeC, valueinSeconds) {
    try {
        timeC.start({
            countdown: true,
            startValues: { seconds: valueinSeconds }
        });
    } catch (e) {
        logger.error("Error iniciando timer: " + e);
    }
}

/***********************************************************************************************************
 *********************************************** SULLA  *****************************************************
 ***********************************************************************************************************/

function runSulla() {
    //eliminar carpeta session al iniciar
    fs.remove("./session")
        .then(() => {
            logger.info("Session borrada OK");
            //iniciar sulla
            sulla.create().then(client => {
                clientSulla = client;
            });
            ev.on("qr", async qrcode => {
                const imageBuffer = Buffer.from(
                    qrcode.replace("data:image/png;base64,", ""),
                    "base64"
                );
                fs1.writeFileSync("src/public/qr_code.png", imageBuffer);
            });
        })
        .catch(err => {
            logger.error("Problemas borrando el direcctorio session: ", err);
        });

    setTimeout(function() {
        logger.info("Ejecutando cliente sulla");
        /*
  Ejecución de cliente de sulla para escuchar mensajes
*/
        if (clientSulla) {
            io.sockets.emit("err_qr", 2);
            clientSulla.onMessage(async function(message) {
                let idUserWa = message.from;
                let nameWa = message.sender.pushname;
                let messWa = message.body;
                let result = userWhatsapp.find(soc => soc.idSocket === idUserWa);

                let a = await checkandSave(result, idUserWa, nameWa);

                let pos = searchIndexWhatsapp(idUserWa);

                let sockidfor = userWhatsapp[pos].idSocket;
                /*
          Dialogflow
  */
                if (userWhatsapp[pos].bot === false) {
                    axios.post(
                        pathUrlTask + "/saveMessage", {
                            idUser: userWhatsapp[pos].id,
                            message: messWa,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + userWhatsapp[pos].jwt
                            }
                        }
                    );
                    var sessionId = "{from}".format(message);
                    var messages = message.body;
                    //const obj = await detectTextIntent(projectId, sessionId, messages, languageCode);
                    var actionQuery = "human";

                    await SaveMessagesDialogFlow(obj);

                    /*
    Asignación de agente desde whatsapp
  */
                    if (actionQuery == "human") {
                        if (timeStart) {
                            userWhatsapp[pos].bot = true;
                            asignAgentWhat(idUserWa, {
                                id: userWhatsapp[pos].id,
                                socketId: idUserWa,
                                channel: "whatsapp"
                            });
                        } else {
                            isHabilDay().then(data => {
                                timeStart = data.hourstart;
                                timeStop = data.hourend;
                                logger.info(
                                    "Horario de atención del día de hoy inicia;" +
                                    timeStart +
                                    " finalzia: " +
                                    timeStop
                                );
                                userWhatsapp[pos].bot = true;
                                asignAgentWhat(idUserWa, {
                                    id: userWhatsapp[pos].id,
                                    socketId: idUserWa,
                                    channel: "whatsapp"
                                });
                            });
                        }
                    }
                    try {
                        var mensajes = obj[0].queryResult.fulfillmentMessages;
                        mensajes.forEach(function(e) {
                            if (e.platform === "PLATFORM_UNSPECIFIED") {
                                try {
                                    let textWa = e.text.text[0];
                                    if (textWa) {
                                        axios.post(
                                            pathUrlTask + "/saveMessage", {
                                                idUser: userWhatsapp[pos].id,
                                                message: textWa,
                                                from: "bot"
                                            }, {
                                                headers: {
                                                    "Content-Type": "application/json",
                                                    authorization: "Bearer " + userWhatsapp[pos].jwt
                                                }
                                            }
                                        );
                                    }
                                    start(clientSulla, idUserWa, textWa);
                                } catch {}
                            }
                        });
                    } catch {
                        logger.error("Error, no hay resultado del API de Dialogflow");
                    }
            /*
    Plataforma BotHUman
  */
                } else {
                    userWhatsapp[pos].timeChat.reset();
                    userWhatsapp[pos].timeChatFin.stop();
                    if (messWa && messWa.toLowerCase() === "hablar con monty bot") {
                        let pos = searchIndexChatPen(sockidfor);
                        if (pos !== -1) {
                            userWhatsapp[pos].bot = false;
                            start(
                                clientSulla,
                                idUserWa,
                                "Hola nuevamente, escribe menú para continuar"
                            );
                        }
                        chatsPendientes.splice(pos, 1);
                    }
                    axios.post(
                        pathUrlTask + "/saveMessageAgent", {
                            idUser: userWhatsapp[pos].id,
                            idSocket: userWhatsapp[pos].idCon,
                            message: messWa,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + userWhatsapp[pos].jwt
                            }
                        }
                    );
                    io.sockets.in(userWhatsapp[pos].idSocket).emit("private-message", {
                        socketIdUser: userWhatsapp[pos].idSocket,
                        messageUser: messWa,
                        idCon: userWhatsapp[pos].idCon
                    });
                }
            });
        } else {
            io.sockets.emit("err_qr", 1);
        }
    }, 25000);
}

async function SaveMessagesDialogFlow(obj) {
    try {
        // Guarda mensajes de DialogFlow
        if (!obj[0].queryResult.fulfillmentText || obj[0].queryResult.fulfillmentText != "" || obj[0].queryResult.fulfillmentText != null || (obj[0].queryResult.fulfillmentText).length != 0) {

            setTimeout(() => {
                tasksFunctions.saveMessage(
                    userWhatsapp[pos].jwt,
                    userWhatsapp[pos].id,
                    obj[0].queryResult.fulfillmentText,
                    "agent"
                )
            }, 0500);
        }
    } catch (error) {
        console.error(error);
    }
}
    

/*
Guardado de Usuario en Base de Datos
*/
async function checkandSave(result, idUserWa, nameWa) {
    if (result === undefined) {
        return await axios
            .post(pathUrlTask + "/callUserWhatsapp", {
                tokenUser: tokenAmd,
                idUser: idUserWa
            })
            .then(async function(resp) {
                if (resp.data) {
                    let idUs = resp.data.id;
                    await axios
                        .post(pathUrlTask + "/reloadUserWa", {
                            tokenUser: tokenAmd,
                            id: idUserWa,
                            name: nameWa
                        })
                        .then(function(resp) {
                            userWhatsapp.push({
                                id: idUs,
                                name: nameWa,
                                idSocket: idUserWa,
                                idCon: 0,
                                jwt: resp.data.token,
                                bot: false,
                                timeChat: new Timer(),
                                timeChatFin: new Timer()
                            });
                            return "seguir";
                        });
                } else {
                    await axios
                        .post(pathUrlTask + "/saveUser", {
                            name: nameWa,
                            acount: idUserWa
                        })
                        .then(resp => {
                            userWhatsapp.push({
                                id: resp.data.data[0].id,
                                name: nameWa,
                                idSocket: idUserWa,
                                idCon: 0,
                                jwt: resp.data.token,
                                bot: false,
                                timeChat: new Timer(),
                                timeChatFin: new Timer()
                            });
                            return "seguir";
                        });
                }
            })
            .catch(e => {
                logger.error("aca:" + e);
                return "seguir";
            });
    } else {
        return "seguir";
    }
}
/*
  envio de mensajes con cliente Sulla
*/
function start(client, idUserWa, messWa) {
    client.sendText(idUserWa, messWa);
}
/*
Busqueda de indice en array de usuarios Whatsapp
*/
function searchIndexWhatsapp(value) {
    pos = userWhatsapp.findIndex(ele => ele.idSocket === value);
    return pos;
}
/*
  Asignamiento agente para Whatsapp
*/
function asignAgentWhat(authorMess, payload) {
    var hour = convertToInt(nowHour());
    if (hour >= timeStart && hour <= timeStop) {
        var result = agentOnline.filter(
            soc =>
            soc.state === "ONLINE" &&
            soc.connect === true &&
            soc.numCon < soc.capacity
        );
        if (result === undefined || result.length === 0) {
            var author = authorMess;
            var text =
                "En este momento no tenemos agentes disponibles, por favor espera un momento.";
            chatsPendientes.push(payload);
            setTimeout(function() {
                start(clientSulla, author, text);
            }, 2500);
        } else {
            try {
                let x = findMinCon(result);
                io.sockets.in(result[x].idAgent).emit("event", payload, "whatsapp");
                let pos = searchIndexAgent(result[x].idAgent);
                agentOnline[pos].numCon += 1;
            } catch (e) {
                logger.error("Error asignando conversacion al agente: " + e);
            }
        }
    } else {
        var author = authorMess;
        let pos = searchIndexWhatsapp(authorMess);
        userWhatsapp[pos].bot = false;
        var text =
            "En este momento estamos fuera del horario de atención, podras comunicarte con un agente de " +
            convertToHour(timeStart) +
            " hasta " +
            convertToHour(timeStop) +
            ".\nSi deseas, puedes seguir hablando con el bot";
        setTimeout(function() {
            start(clientSulla, author, text);
        }, 2500);
    }
}
/*****************************************************************/