import io from "socket.io-client";
import { html, PolymerElement } from "@polymer/polymer/polymer-element.js";
import "@granite-elements/granite-bootstrap/granite-bootstrap.js";
import {} from "@polymer/polymer/lib/utils/resolve-url.js";
import "@polymer/paper-icon-button/paper-icon-button.js";
import "@polymer/paper-dialog/paper-dialog.js";
import "./css/style.css?name=polychat";
import "./css/chat.css?name=chatstyle";
import axios from "axios";
//variables para generación de hora actual y de id unico
const uuid = require("uuid");
/*
    ********************************************************************************************************************
    establecimiento de la variable socket para hacer uso de la función emit del modulo socket i.o
    ********************************************************************************************************************
*/
const pathUrl = "http://localhost:5000";
const nomlocalStor = "data_user_bot_upb";
const nomIdConStor = "idCon_upb";
const nomCountStor = "count_upb";
const socket = io.connect(pathUrl, {
    path: "/widget/upb/socket.io",
    forceNew: true
});
socket.off("private-message");
socket.off("new-message");

var count = 0;
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-chat, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebChat extends PolymerElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open", delegatesFocus: false });
    }

    static get template() {
        return html `
    <style include = "polychat"></style>
   
    <section class="avenue-messenger">
      <div class="menu">
      <div class="items"><span>
        <a href="#" title="Minimize">&mdash;</a><br>
        <a href="#" title="End Chat">&#10005;</a>
       
        </span></div>
        <div on-click="showHide" class="button"><img width="15px" height="15px" src="{{imgclosewindow}}" /></div>
      </div>
    <div class="chat">        
      <div class="chat-title">
        <img style="padding-bottom:15px;" src="{{imgmontechelo}}" />
      </div>
      <div class="messages">

        <div class="messages-content" id="boxMess"></div>
      </div>
      <div class="message-box">
      <textarea maxlength="250" on-click="downWin" id="private_message" type="text" class="message-input" placeholder="Escribe tu mensaje..." on-keydown="sendMessage"></textarea>
        <button type="submit" class="message-submit" on-click="sendMessage"><img width="30px" src="{{imgsrcbtn}}"></button>
      </div>
      <div class="fotstyle">
      <p class="pstyle">Powered By <a style="text-decoration: none !important; color:#FFFFFF; " href="https://www.montechelo.com.co/" target="_blank">Montechelo</a></p>
      </div>
    </div>
      </div>
    `;
    }

    static get properties() {
        return {
            list: {
                type: Array,
                value: []
            },
            imgsrc: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/boticon.png");
                }
            },
            imgsrcbtn: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/send.svg");
                }
            },
            imgupb: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/upb-log.png");
                }
            },
            imgiconbot: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/upb_bot_chat.svg");
                }
            },
            imgactive: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/Activo_Top.svg");
                }
            },
            imgchatbot: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/upb_bot_chat.svg");
                }
            },
            imgchatuser: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/upb_user_chat.svg");
                }
            },
            imgclosewindow: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/close.png");
                }
            }
        };
    }

    ready() {
        super.ready();
        this.runOncePerDay();

        window.mess = this.$.messages;
        window.boxMess = this.$.boxMess;
        window.divv = this.$.winMessages;

        window.prvmess = this.$.private_message;
        //Si usuario ya se encuentra registrado
        if (JSON.parse(localStorage.getItem(nomlocalStor))) {
            window.idUser = JSON.parse(localStorage.getItem(nomlocalStor)).id;
            window.idToken = JSON.parse(localStorage.getItem(nomlocalStor)).token;
            window.idTokenJWT = JSON.parse(localStorage.getItem(nomlocalStor)).jwt;

            axios
                .post(
                    pathUrl + "/widget/upb/tasks/callAllConversation", {
                        idUser: window.idUser
                    }, {
                        headers: {
                            "Content-Type": "application/json",
                            authorization: "Bearer " + window.idTokenJWT
                        }
                    }
                )
                .then(res => {
                    res.data.forEach(e => {
                        let node = document.createElement("div");
                        if (e.who == "user") {
                            renderMessageUser(node, e.message, e.hora);
                        } else if (e.who == "bot") {
                            renderMessageBot(node, e.message, e.hora);
                        } else if (e.who == "agent") {
                            renderMessageAgent(node, e.message, e.hora);
                        }
                    });
                    window.boxMess.scrollTop = window.boxMess.scrollHeight;
                });

            if (localStorage.getItem(nomIdConStor)) {
                window.idConversation = localStorage.getItem(nomIdConStor);
                axios
                    .post(
                        pathUrl + "/widget/upb/tasks/statusConvertation", {
                            idCon: localStorage.getItem(nomIdConStor)
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(res => {
                        if (res.data[0].estado != "Abierta") {
                            localStorage.setItem(nomCountStor, 0);
                            localStorage.removeItem(nomIdConStor);
                        }
                    });
            }
        }

        if (parseInt(localStorage.getItem(nomCountStor)) == 3) {
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                null,
                "webchat",
                "user"
            );
        }
    }
    hasOneDayPassed() {
        // get today's date. eg: "7/37/2007"
        var date = new Date().toLocaleDateString();
        // if there's a date in localstorage and it's equal to the above:
        // inferring a day has yet to pass since both dates are equal.
        if (localStorage.getItem("date_app_upb") == date) return false;

        // this portion of logic occurs when a day has passed
        localStorage.setItem("date_app_upb", date);
        return true;
    }

    // some function which should run once a day
    runOncePerDay() {
        if (!this.hasOneDayPassed()) return false;
        localStorage.removeItem(nomlocalStor);
        localStorage.removeItem(nomCountStor);
        localStorage.removeItem(nomIdConStor);
    }
    _isEqual(list, string) {
        return list == string;
    }
    downWin() {
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
    }
    reset() {
        localStorage.setItem(nomCountStor, 0);
    }
    sendMessageMenu() {
        if (localStorage.getItem(nomCountStor) < 3) {
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            var node = document.createElement("div");
            renderMessageUser(node, "Menú", nowHour());
            socket.emit("new-message", {
                message: "menú",
                id: JSON.parse(localStorage.getItem(nomlocalStor)).token
            });
        }
    }
    sendMessage(evt) {
        var payload = this.$.private_message.value;
        if ((evt.keyCode == 13 || evt.type == "click") && payload.trim() != "") {
            evt.preventDefault();
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            var node = document.createElement("div");
            renderMessageUser(node, payload, nowHour());
            if (
                localStorage.getItem(nomCountStor) == "0" ||
                localStorage.getItem(nomCountStor) == "1"
            ) {
                socket.emit("new-message", {
                    message: payload,
                    id: JSON.parse(localStorage.getItem(nomlocalStor)).token
                });
                axios
                    .post(
                        pathUrl + "/widget/upb/tasks/saveMessage", {
                            idUser: window.idUser,
                            message: payload,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(() => {
                        this.downWin();
                    });
            } else if (localStorage.getItem(nomCountStor) == "2") {
                socket.emit("Agent", "Chats", {
                    socketId: JSON.parse(localStorage.getItem(nomlocalStor)).token,
                    id: window.idUser,
                    channel: "webchat"
                });
                axios
                    .post(
                        pathUrl + "/widget/upb/tasks/saveMessage", {
                            idUser: window.idUser,
                            message: payload,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(() => {
                        this.downWin();
                    });
                var node2 = document.createElement("div");
                renderMessageBot(
                    node2,
                    "En un momento un agente se comunicara contigo",
                    nowHour()
                );
                count = parseInt(localStorage.getItem(nomCountStor)) + 1;
                localStorage.removeItem(nomCountStor);
                localStorage.setItem(nomCountStor, count);
                socket.emit(
                    "Agent-User",
                    JSON.parse(localStorage.getItem(nomlocalStor)).token,
                    null
                );
            } else {
                socket.emit(
                    "Agent-User",
                    JSON.parse(localStorage.getItem(nomlocalStor)).token,
                    payload,
                    null,
                    window.idConversation,
                    "webchat",
                    "tmoReset", {
                        id: JSON.parse(localStorage.getItem(nomlocalStor)).id,
                        name: JSON.parse(localStorage.getItem(nomlocalStor)).name,
                        email: JSON.parse(localStorage.getItem(nomlocalStor)).email,
                        jwt: JSON.parse(localStorage.getItem(nomlocalStor)).jwt
                    }
                );
                axios
                    .post(
                        pathUrl + "/widget/upb/tasks/saveMessageAgent", {
                            idUser: window.idUser,
                            idSocket: window.idConversation,
                            message: payload,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    )
                    .then(() => {
                        this.downWin();
                    });
            }
            this.$.private_message.value = "";
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        }
    }
    showHide() {
        var displayed = window.cchat2.style.display;
        window.segundo.style.visibility = "hidden";
        window.segundo.style.display = "none";
        window.primero.style.visibility = "visible";
        window.primero.style.display = "block";
        if (displayed == "none") {
            window.cchat2.style.display = "";
        } else {
            window.cchat2.style.display = "none";
        }
    }
}
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-form, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebForm extends PolymerElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open", delegatesFocus: false });
    }

    static get template() {
        return html `
    <style include = "polychat">
    </style>
    <section class="avenue-messenger">
      <div class="menu">
      <div class="items"><span>
        <a href="#" title="Minimize">&mdash;</a><br>
        <a href="#" title="End Chat">&#10005;</a>        
        </span></div>
        <div on-click="showHide" class="button"><img width="15px" height="15px" src="{{imgclosewindow}}" /></div>
      </div>
    <div class="chat">
      <div class="chat-title">
      <img style="margin-top: -18px; height: 112px" src="{{imgupb}}" />
      </div>
      <div class="messages">
      <!--inicio form-->
      <paper-dialog id="modalTermsCon" class="size-modalTerms">
      <div style="text-align:justify; text-justify: inter-word;">
      De conformidad con lo establecido por la Ley 1581 de 2012, el Decreto Reglamentario 1074 de 2015 y las demás normas que las modifiquen y/o amplíen, autorizo de manera libre, expresa e informada a la Universidad Pontificia Bolivariana para recolectar, almacenar, circular y utilizar los datos personales suministrados mediante el presente formato. El tratamiento de los datos personales estará sujeto a las siguientes finalidades: i) Comunicar mediante correos electrónicos, redes sociales, plataformas virtuales propias o determinadas por la institución, mensajes de texto, aplicaciones de mensajería instantánea y/o llamadas telefónicas, información institucional y/o promocional de la Universidad Pontificia Bolivariana. ii) Establecer contacto mediante llamadas telefónicas, mensajes de texto, aplicaciones de mensajería instantánea, redes sociales y/o correos electrónicos, con el fin de ofertar los programas académicos de pregrado y posgrado de la universidad, así como las demás actividades y capacitaciones académicas que desarrollen las diferentes áreas de la Universidad Pontificia Bolivariana, iii) Recibir información comercial propia de la actividad de la Institución. Manifiesto que he consultado en https://www.upb.edu.co el Manual de Políticas de Tratamiento de Información y Protección de los Datos Personales y certifico que conozco sobre mis derechos para solicitar la eliminación, rectificación, actualización y supresión de mis datos personales, mediante los canales dispuestos por la universidad        </div>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTermsCon" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalCorreo" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>correo eléctronico</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalEmail" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalTelefono" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>número de teléfono</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTelefono" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalCard" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>número de identificacion</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalCard" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalOfficeHour" class="size-modalData">
        <p class="paragraphModal">En este momento estamos fuera del horario de atención, podras comunicarte con un agente de <strong>Lunes a Viernes 07:00 am a 07:00 pm</strong> y <strong>Sábados 08:00 am a 12 del mediodía.</strong></p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalOffice" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalData" class="size-modalData">
        <p class="paragraphModal">Por favor, dime tu <strong>nombre</strong>, <strong>número de teléfono</strong></p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModal" value="Cerrar">         
        </div>
      </paper-dialog>
      <paper-dialog id="modalTerms" class="size-modalData">
        <p class="paragraphModal">Para continuar con nuestro servicio debes aceptar <strong>terminos y condiciones</strong> </p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTerms" value="Cerrar">         
        </div>
      </paper-dialog>
      <div class="messages-content">
        
          <img src="{{imgiconbot}}" class="imgstyle"/>
          <p class="greetings">¡Hola!</p>
          <p class="paragraph">Por favor <strong>completa la siguiente información</strong> para chatear con un asesor.</p>
          <br/>
          <div class="divForm" style="">
          <form class="formFlex">
              <label for="name" class="labelForm">Nombres y Apellidos <span style="color:#ff7a04">*</span></label>
              <input type="text" id="name" name="name" class="inputclass" placeholder="Escribe tu nombre">
           
              
              <label for="email" class="labelForm">Correo electrónico</label>
              <input type="email" id="email" name="email" class="inputclass" placeholder="Escribe tu email">
             
            <input id="defaultCheck1" class="check" type="checkbox" name="terminos" value="terminos"><span on-click="openTerms" style="cursor: pointer; color: #02000A;">  Autorizo <span style="text-decoration:underline;">el registro y uso de mis datos personales y acepto la política de protección de datos personales mencionada Aquí.*</span></span><span style="color:#ff7a04">*</span><br>
            <input class="buttonContinue" type="button" on-click="submit" value="Continuar">            
          </div>
        </div>
      </div>
      <!--fin form -->
      <div class="fotstyle">
        <p class="pstyle">Powered By <a style="text-decoration: none !important; color:#FFFFFF; " href="https://www.montechelo.com.co/" target="_blank">Montechelo</a></p>
      </div>
    </div>
      </div>
    `;
    }

    static get properties() {
        return {
            imgupb: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/upb-log.png");
                }
            },
            imgiconbot: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/avatar.png");
                }
            },
            imgactive: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/Activo_Top.svg");
                }
            },
            imgclosewindow: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/close.png");
                }
            },
            otrosProg: {
                type: String,
                value: "none"
            },
            otrosSede: {
                type: String,
                value: "none"
            }
        };
    }

    ready() {
        super.ready();
    }

    showHide() {
        window.segundo.style.visibility = "hidden";
        window.segundo.style.display = "none";
        window.primero.style.visibility = "visible";
        window.primero.style.display = "block";
        var displayed = window.cchat.style.display;
        if (displayed == "none") {
            window.cchat.style.display = "";
        } else {
            window.cchat.style.display = "none";
        }
    }
    openTerms() {
        this.$.modalTermsCon.open();
    }

    hideModalTermsCon() {
        this.$.modalTermsCon.toggle();
    }

    hideModal() {
        this.$.modalData.toggle();
    }

    hideModalTerms() {
        this.$.modalTerms.toggle();
    }
    hideModalEmail() {
        this.$.modalCorreo.toggle();
    }
    hideModalOffice() {
        this.$.modalOfficeHour.toggle();
    }
    hideModalTelefono() {
        this.$.modalTelefono.toggle();
    }
    hideModalCard() {
        this.$.modalCard.toggle();
    }
    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    submit(e) {
        e.preventDefault();
        console.log('Validación de datos de form');
        var nameUser = this.$.name.value;
        var numberDocumentUser = "1030744444";
        var emailUser = this.$.email.value;
        //var phoneUser = this.$.phone.value;
        var terminosUser = this.$.defaultCheck1.checked;

        if (
            nameUser &&
            numberDocumentUser &&
            terminosUser === true
        ) {
            if (isNaN(numberDocumentUser) === true || numberDocumentUser.length < 5) {
                this.$.modalCard.open();
            } else if (emailUser && this.validateEmail(emailUser) === false) {
                this.$.modalCorreo.open();
            } else {
                isHabilDay().then(data => {
                    var hour = convertToInt(nowHour());
                    var dia = new Date();
                    if (
                        hour >= data.hourstart &&
                        hour <= data.hourend &&
                        dia.getDay() != 0
                    ) {
                        axios
                            .post(pathUrl + "/widget/upb/tasks/saveUser", {
                                name: nameUser,
                                numerodocumento: numberDocumentUser,
                                email: emailUser,
                                phone: null,
                                conditions: "Aceptado",
                                canal: "Chat"
                            })
                            .then(response => {
                                let tokenuser = uuid() + nameUser;
                                localStorage.setItem(
                                    nomlocalStor,
                                    JSON.stringify({
                                        id: response.data.data[0].id,
                                        name: nameUser,
                                        email: emailUser,
                                        token: tokenuser,
                                        jwt: response.data.token
                                    })
                                );
                                socket.emit("new-message", {
                                    message: "Hola",
                                    id: JSON.parse(localStorage.getItem(nomlocalStor)).token
                                });
                                this.$.name.value = "";
                                this.$.email.value = "";
                                this.$.defaultCheck1.checked = "";
                                this.$.defaultCheck1.checked = false;
                                window.idUser = response.data.data[0].id;
                                window.idTokenJWT = response.data.token;
                                window.cchat.style.display = "none";
                                window.cchat2.style.display = "";
                            })
                            .catch(e => {
                                console.error(e);
                            });
                    } else {
                        this.$.modalOfficeHour.open();
                    }
                });
            }
        } else if (!nameUser ||
            !numberDocumentUser ||
            !phoneUser 
            ) {
            this.$.modalData.open();
        } else if (terminosUser === false) {
            this.$.modalTerms.open();
        }
    }
}
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-form, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebFormCall extends PolymerElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open", delegatesFocus: false });
    }

    static get template() {
        return html `
    <style include = "polychat">
    </style>
    <section class="avenue-messenger">
      <div class="menu">
      <div class="items"><span>
        <a href="#" title="Minimize">&mdash;</a><br>
        <a href="#" title="End Chat">&#10005;</a>        
        </span></div>
        <div on-click="showHide" class="button"><img width="15px" height="15px" src="{{imgclosewindow}}" /></div>
      </div>
    <div class="chat">
      <div class="chat-title">
      <img style="margin-top: -18px; height: 112px;" src="{{imgupb}}" />
      </div>
      <div class="messages">
      <!--inicio form-->
      <paper-dialog id="modalTermsCon" class="size-modalTerms">
      <div style="text-align:justify; text-justify: inter-word;">
          <p style="margin-right: 0cm; margin-bottom: 7.5pt; margin-left: 0cm; background: white; text-align: center;"><span style="font-size: 24px; font-weight: bold; font-family: 'roboto_slablight',serif; color: #333333;">Políticas de Tratamiento de Información y Protección de los Datos Personales de la Universidad Pontificia Bolivariana</span></p>
          <p style="margin-right: 0cm; margin-bottom: 7.5pt; margin-left: 0cm; background: white;"><span style="font-size: 19px; font-weight: bold; font-family: 'roboto_slablight',serif; color: #333333;">Autorización y aviso de privacidad&nbsp;</span></p>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">Cordial saludo,</span></p>
          <p style="margin-bottom: 7.5pt; background: white; text-align: justify;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">La Universidad Pontificia Bolivariana como responsable del tratamiento de los datos que a través de medios digitales o físicos Usted nos ha permitido almacenar, en nuestras bases de datos, en cumplimiento de lo establecido por las normas vigentes: Ley 1581 de 2012, y Decreto 1377 de 2013, le solicita su autorización para recolectar, almacenar, circular y usar sus datos personales, según lo establecido en el Manual de Políticas de tratamiento de Información y Protección de Datos Personales en la Universidad Pontificia Bolivariana el cual puede ser consultado en el siguiente vínculo:</span></p>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;"><a target="autoblank" href="https://www.upb.edu.co/ss/es/documentos/doc-manualpoliticatratamientoinformacionprotecciondatos-tra-1464106913839.pdf"><span style="color: #ed1c24;">Manual políticas, tratamiento de información y protección de datos personales</span></a> (PDF)</span></p>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">&nbsp;</span></p>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">La información suministrada por Usted, será utilizada única y exclusivamente para los siguientes fines:</span></p>
          <ul>
          <li><em><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Propiciar el seguimiento, acompa&ntilde;amiento, cumplimiento y control de los compromisos contractuales, o el vínculo entre el TITULAR y la UNIVERSIDAD.</span></em></li>
          <li><em><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Dar a conocer la oferta académica y/o de servicios de la Institución.</span></em></li>
          <li><em><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Ofertar los servicios, académicos, clínicos, científicos, investigativos y tecnológicos de la Institución y demás servicios que oferte la UNIVERSIDAD.</span></em></li>
          <li><em><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Remitir información institucional u otra información que se relacione con el objeto del vínculo entre la UNIVERSIDAD y el TITULAR de los datos personales, tales como: noticias, las políticas, procesos, encuestas, actividades, eventos académicos, culturales, sociales, deportivos y de salud entre otros.</span></em></li>
          </ul>
          <p style="margin-bottom: 7.5pt; background: white; text-align: justify;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">Si desea que sus datos sean modificados o suprimidos de nuestra base de datos, podrá notificarlo al correo electrónico:</span></p>
          <ul>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Sede Central Medellín:&nbsp;<a href="mailto:datos.personales@upb.edu.co"><span style="color: #0052CC;">datos.personales@upb.edu.co</span></a></span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Secccional Bucaramanga:&nbsp;<a href="mailto:datos.personales.bga@upb.edu.co"><span style="color: #0052CC;">datos.personales.bga@upb.edu.co</span></a></span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Seccional Montería:&nbsp;<a href="mailto:datos.personales.monteria@upb.edu.co"><span style="color: #0052CC;">datos.personales.monteria@upb.edu.co</span></a></span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Seccional Palmira:<a href="mailto:datos.personales.palmira@upb.edu.co"><span style="color: #0052CC;">datos.personales.palmira@upb.edu.co</span></a></span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Unidad de Gestión y Proyección Bogotá:&nbsp;<a href="mailto:datos.personales.bogota@upb.edu.co"><span style="color: #0052CC;">datos.personales.bogota@upb.edu.co</span></a><a href="mailto:datos.personales.bogota@upb.edu.co%E2%80%8B"></a></span></li>
          </ul>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">Para atención física y personalizada usted puede contactarnos en:</span></p>
          <ul>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Sede Central Medellín. Bloque 15- Gestión Documental (piso cero)</span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Seccional Montería: Carrera 6 # 97A-99, oficina 101 Comunicaciones y Relaciones Públicas</span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Seccional Bucaramanga: &nbsp;Autopista a Piedecuesta Km. 7 Vía a Piedecuesta</span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Seccional Palmira: Km 1 vía a Tienda Nueva - Coordinación de Comunicaciones, 2do. Piso</span></li>
          <li><span style="font-size: 10.5pt; font-family: 'ek_muktaextralight',serif;">Unidad de Gestión y Proyección en Bogotá: Avenida Boyacá No.169 D&ndash; 75 / Celam.</span></li>
          </ul>
          <p style="margin-bottom: 7.5pt; background: white; text-align: justify;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">De acuerdo al tipo de titulares de datos personales, también se tienen fines específicos los cuales usted como titular de la información deberá autorizar a la Universidad por medio de los formatos que la misma indique.</span></p>
          <p style="margin-bottom: 7.5pt; background: white; text-align: justify;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">La Universidad le garantiza seguridad y confidencialidad en el manejo de su información. Es muy importante para nosotros mantener la relación con usted, así mismo contarle sobre los avances, desarrollos y opciones de formación y transformación.</span></p>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">Atentamente,</span></p>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">&nbsp;</span></p>
          <p style="margin-bottom: 7.5pt; background: white;"><span style="font-size: 12.0pt; font-family: 'ek_muktaextralight',serif; color: #333333;">Pbro. Julio Jairo Ceballos Sepúlveda<br />Rector General<br />Universidad Pontificia Bolivariana</span></p>
        </div>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTermsCon" value="Cerrar">        
        </div>
      </paper-dialog>
      <paper-dialog id="modalCorreo" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>correo eléctronico</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalEmail" value="Cerrar">        
        </div>
      </paper-dialog>
      <paper-dialog id="modalTelefono" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>número de teléfono</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTelefono" value="Cerrar">        
        </div>
      </paper-dialog>
      <paper-dialog id="modalCard" class="size-modalData">
        <p class="paragraphModal">Debes ingresar un <strong>número de identificacion</strong> valido</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalCard" value="Cerrar">        
        </div>
      </paper-dialog>
      <paper-dialog id="modalOfficeHour" class="size-modalData">
        <p class="paragraphModal">En este momento estamos fuera del horario de atención, podras comunicarte con un agente de <strong>Lunes a Viernes 07:00 am a 07:00 pm</strong> y <strong>Sábados 08:00 am a 12 del mediodía.</strong></p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalOffice" value="Cerrar">        
        </div>
      </paper-dialog>
      <paper-dialog id="modalData" class="size-modalData">
        <p class="paragraphModal">Por favor, dime tu <strong>nombre</strong>, <strong>tipo y número de identificación</strong>, <strong>número de teléfono</strong> y <strong>sede, seccional u otro lugar UPB</strong></p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModal" value="Cerrar">        
        </div>
      </paper-dialog>
      <paper-dialog id="modalTerms" class="size-modalData">
        <p class="paragraphModal">Para continuar con nuestro servicio debes aceptar <strong>terminos y condiciones</strong> </p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalTerms" value="Cerrar">        
        </div>
      </paper-dialog>
      <paper-dialog id="modalLLamada" class="size-modalData">
        <p class="paragraphModal">En un momento un <strong>asesor Call Center</strong> se comunicará contigo.</p>
        <div class="buttons">
          <input class="buttonClose" type="button" on-click="hideModalLlamada" value="Cerrar">        
        </div>
      </paper-dialog>
        <div class="messages-content">
          <img src="{{imgiconbot}}" class="imgstyle"/>
          <p class="greetings">¡Hola!</p>
          <p class="paragraph">Por favor <strong>completa la siguiente información</strong> para comunicarte con un asesor Call Center.</p>
          <br/>
          <div class="divForm" style="">
            <form class="formFlex">
              <label for="name" class="labelForm">Nombres y Apellidos <span style="color:#ff7a04">*</span></label>
              <input type="text" id="name" name="name" class="inputclass" placeholder="Escribe tu nombre">
              <label for="iden" class="labelForm">Tipo de Documento de identidad <span style="color:#ff7a04">*</span></label>
              <select class="selectclass" id="iden" name="iden">
                <option selected value="">Seleciona alguna...</option>
                <option value="T.I">Tarjeta de Identidad</option>
                <option value="C.C">Cédula de ciudadanía</option>
                <option value="C.E">Cédula extranjería</option>
                <option value="Pasaporte">Pasaporte</option>
                <option value="Nuip">Nuip</option>
              </select>
              <label for="documento" class="labelForm">Número de Documento de identidad <span style="color:#ff7a04">*</span></label>
              <input type="text" id="documento" name="documento" class="inputclass" placeholder="Escribe tu número de documento">
              <label for="email" class="labelForm">Correo electrónico</label>
              <input type="email" id="email" name="email" class="inputclass" placeholder="Escribe tu email">
              <label for="phone" class="labelForm">Celular <span style="color:#ff7a04">*</span></label>
              <input type="text" id="phone" name="phone" class="inputclass" placeholder="Escribe tu celular">
              <label for="tipsol" class="labelForm">Tipo de solicitud</label>              
              <select class="selectclass" id="tipsol" name="tipsol">
                <option selected value="">Seleciona alguna...</option>
                <option value="Solicitud de información">Solicitud de información</option>
                <option value="Peticion">Petición</option>
                <option value="Queja">Queja</option>
                <option value="Reclamo">Reclamo</option>
                <option value="Sugerencia">Sugerencia</option>
                <option value="Felicitacion">Felicitación</option>
              </select>
              <label for="progint" class="labelForm">Programa o servicio de tú interés</label>              
              <select class="selectclass" id="progint" name="progint" on-click="checkProg">
                <option selected value="">Seleciona alguna...</option>
                <option  value="Programas profesionales">Programas profesionales</option>
                <option  value="Postgrados">Postgrados</option>
                <option  value="Formacion Continua">Formación Continua</option>
                <option  value="Colegio">Colegio</option>
                <option  value="Centro de Lenguas">Centro de Lenguas</option>
                <option  value="Programas virtuales">Programas virtuales</option>
                <option  value="Programas o servicios para empresas">Programas o servicios para empresas</option>
                <option  value="otros" >Otros</option>
              </select>
              <label style$="display: {{otrosProg}};" for="otrosprog" class="labelForm">Otros programas de interes</label>
              <textarea maxlength="250" style$="display: {{otrosProg}};" type="text" id="otrosprog" name="otrosprog" class="inputTextarea" placeholder="Escribe otro programa"></textarea>
              <label for="sede" class="labelForm">Sede, Seccional u otro lugar UPB <span style="color:#ff7a04">*</span></label>              
              <select class="selectclass" id="sede" name="sede" on-click="checkSede">
                <option selected value="">Seleciona alguna...</option>
                <option value="Medellin">Medellín (incluye Robledo y El Poblado)</option>
                <option value="Oriente antioqueño">Oriente antioqueño</option>
                <option value="Bogota">Bogotá</option>
                <option value="Armenia">Armenia</option>
                <option value="otros">Otros</option>
              </select>
              <label  style$="display: {{otrosSede}};" for="otroslug" class="labelForm">Otro lugar</label>
              <textarea maxlength="250" style$="display: {{otrosSede}};"  type="text" id="otroslug" name="otroslug" class="inputTextarea" placeholder="Escribe otro lugar"></textarea>              
              <label for="commentary" class="labelForm">Comentario</label>
              <textarea maxlength="250" type="text" id="commentary" name="commentary" class="inputTextarea" placeholder="Escribe tu comentario"></textarea>
            </form>
              <input id="defaultCheck1" class="check" type="checkbox" name="terminos" value="terminos"><span on-click="openTerms" style="cursor: pointer; color: #4da9cc;"> Aceptar <span style="text-decoration:underline;">terminos y condiciones</span></span><span style="color:#ff7a04">*</span><br>
              <input class="buttonContinue" type="button" on-click="submit" value="Continuar">            
          </div>
        </div>
      </div>      
      <!--fin form -->
      <div class="fotstyle">
        <p class="pstyle">Powered By <a style="text-decoration: none !important; color:#FFFFFF; " href="https://www.montechelo.com.co/" target="_blank">Montechelo</a></p>
      </div>
    </div>
      </div>
    `;
    }

    static get properties() {
        return {
            imgupb: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/upb-log.png");
                }
            },
            imgiconbot: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/formCall.svg");
                }
            },
            imgactive: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/Activo_Top.svg");
                }
            },
            imgclosewindow: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/close.png");
                }
            },
            imgsrcall: {
                type: String,
                value: function() {
                    return this.resolveUrl(
                        pathUrl + "/widget/upb/img/upb_button_call.svg"
                    );
                }
            },
            otrosProg: {
                type: String,
                value: "none"
            },
            otrosSede: {
                type: String,
                value: "none"
            }
        };
    }

    ready() {
        super.ready();
    }

    showHide() {
        window.segundoCall.style.visibility = "hidden";
        window.segundoCall.style.display = "none";
        window.primeroCall.style.visibility = "visible";
        window.primeroCall.style.display = "block";
        window.FormCall.style.display = "none";
    }

    openTerms() {
        this.$.modalTermsCon.open();
    }

    hideModalTermsCon() {
        this.$.modalTermsCon.toggle();
    }
    hideModal() {
        this.$.modalData.toggle();
    }
    hideModalTerms() {
        this.$.modalTerms.toggle();
    }
    hideModalEmail() {
        this.$.modalCorreo.toggle();
    }
    hideModalOffice() {
        this.$.modalOfficeHour.toggle();
    }
    hideModalTelefono() {
        this.$.modalTelefono.toggle();
    }
    hideModalCard() {
        this.$.modalCard.toggle();
    }
    hideModalLlamada() {
        this.$.modalLLamada.toggle();
    }
    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    checkProg() {
        if (this.$.progint.value == "otros") {
            return (this.otrosProg = "");
        } else {
            return (this.otrosProg = "none");
        }
    }
    checkSede() {
        if (this.$.sede.value == "otros") {
            return (this.otrosSede = "");
        } else {
            return (this.otrosSede = "none");
        }
    }
    submit(e) {
        e.preventDefault();

        var nameUser = this.$.name.value;
        var tipoDocumentUser = this.$.iden.value;
        var numberDocumentUser = this.$.documento.value;
        var emailUser = this.$.email.value;
        var phoneUser = this.$.phone.value;
        var tipoSolUser = this.$.tipsol.value;
        var progInteUser = this.$.progint.value;
        var otrosProgUser = this.$.otrosprog.value;
        var sedeUser = this.$.sede.value;
        var otrosSedeUser = this.$.otroslug.value;
        var commentaryUser = this.$.commentary.value;
        var terminosUser = this.$.defaultCheck1.checked;
        if (
            nameUser &&
            tipoDocumentUser &&
            numberDocumentUser &&
            phoneUser &&
            sedeUser &&
            terminosUser === true
        ) {
            if (isNaN(numberDocumentUser) === true || numberDocumentUser.length < 5) {
                this.$.modalCard.open();
            } else if (emailUser && this.validateEmail(emailUser) === false) {
                this.$.modalCorreo.open();
            } else if (isNaN(phoneUser) === true || phoneUser.length < 10) {
                this.$.modalTelefono.open();
            } else if (
                sedeUser == "otros" &&
                (otrosSedeUser.trim() == "" || otrosSedeUser == null)
            ) {
                this.$.modalData.open();
            } else {
                isHabilDay().then(data => {
                    var hour = convertToInt(nowHour());
                    var dia = new Date();
                    if (
                        hour >= data.hourstart &&
                        hour <= data.hourend &&
                        dia.getDay() != 0
                    ) {
                        var vm = this;
                        axios
                            .post(pathUrl + "/widget/upb/tasks/saveUser", {
                                name: nameUser,
                                tipodocumento: tipoDocumentUser,
                                numerodocumento: numberDocumentUser,
                                email: emailUser,
                                phone: phoneUser,
                                tiposolicitud: tipoSolUser,
                                proginteres: progInteUser,
                                otroprog: otrosProgUser,
                                sedeusuario: sedeUser,
                                otrasede: otrosSedeUser,
                                commentary: commentaryUser,
                                conditions: "Aceptado",
                                canal: "Llamada"
                            })
                            .then(() => {
                                axios
                                    .post(pathUrl + "/widget/upb/tasks/vicidial", {
                                        name: nameUser,
                                        telefono: phoneUser,
                                        email: emailUser,
                                        sede: sedeUser,
                                        commentary: commentaryUser,
                                        id: numberDocumentUser,
                                        tipId: tipoDocumentUser,
                                        tipoSol: tipoSolUser
                                    })
                                    .then(function() {
                                        vm.$.name.value = "";
                                        vm.$.iden.value = "";
                                        vm.$.documento.value = "";
                                        vm.$.email.value = "";
                                        vm.$.phone.value = "";
                                        vm.$.tipsol.value = "";
                                        vm.$.progint.value = "";
                                        vm.$.otrosprog.value = "";
                                        vm.$.sede.value = "";
                                        vm.$.otroslug.value = "";
                                        vm.$.commentary.value = "";
                                        vm.$.defaultCheck1.checked = "";
                                        vm.$.defaultCheck1.checked = false;
                                    });
                            })
                            .catch(e => {
                                console.error(e);
                            });
                    } else {
                        this.$.modalOfficeHour.open();
                    }
                });
            }
            this.$.modalLLamada.open();
        } else if (!nameUser ||
            !numberDocumentUser ||
            !tipoDocumentUser ||
            !phoneUser ||
            !sedeUser
        ) {
            this.$.modalData.open();
        } else if (terminosUser === false) {
            this.$.modalTerms.open();
        }
        axios({
            method: 'GET',
            url: 'https://dashbots.outsourcingcos.com/api_vicidial/general?url=172.80.7.211&usuario=1040745468&password=1040745468&list_id=12000&nombre=' + nameUser + '&telefono=' + phoneUser + '&nombre=prueba&comments=' + commentaryUser
        }).then(res => {
            console.log(res)
        }).catch(err => console.log(err))
    }
}
/*
    ********************************************************************************************************************
    Clase que extiende del PolymerElement, define el html del custom element web-icon, sus propiedades y functiones
    ********************************************************************************************************************
*/
class WebIcon extends PolymerElement {
    constructor() {
        super();
        this.attachShadow({ mode: "open", delegatesFocus: false });
    }

    static get template() {
        return html `
      <style>
        .buttonChat {
          position: fixed;
          bottom: 10px;
          right: 10px;
          border: none;
          color: white;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 4px 2px;
          cursor: pointer;
          z-index: 999;
        }
        .buttonChatCall {
          position: fixed;
          bottom: 80px;
          right: 10px;
          border: none;
          color: white;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 16px;
          margin: 4px 2px;
          cursor: pointer;
          z-index: 999;
        }
      </style>
      <style include="granite-bootstrap"></style>
      <style include="chatstyle"></style>

      <web-form
        id="chatWin"
        style="border-radius: 50px 20px; display:none"
      ></web-form>
      <web-chat
        id="chatWin2"
        style="border-radius: 50px 20px; display:none"
      ></web-chat>
      <web-form-call
        id="chatWinForm"
        style="border-radius: 50px 20px; display:none"
      ></web-form-call>

      <div class="buttonChat bubble">
        <p
          id="primero"
          style="visibility:visible; display:block;"
          on-click="visualiza_segundo"
        >
          <img src="{{imgsrchat}}" style="width:30px; margin-top:15px;" />
        </p>
        <p
          id="segundo"
          style="visibility:hidden; display:none;"
          on-click="visualiza_primero"
        >
          <img src="{{imgsrclose}}" style="width:25px; margin-top:18px;" />
        </p>
      </div>
      <div class="buttonChatCall bubble">
        <p
          id="primerocall"
          style="visibility:visible; display:block;"
          on-click="visualiza_segundo_call"
        >
          <img src="{{imgsrcall}}" style="width:30px; margin-top:15px;" />
        </p>
        <p
          id="segundocall"
          style="visibility:hidden; display:none;"
          on-click="visualiza_primero_call"
        >
          <img src="{{imgsrclose}}" style="width:25px; margin-top:18px;" />
        </p>
      </div>
    `;
    }

    ready() {
        super.ready();
        window.cchat = this.$.chatWin;
        window.cchat2 = this.$.chatWin2;
        window.FormCall = this.$.chatWinForm;
        window.primero = this.$.primero;
        window.segundo = this.$.segundo;
        window.primeroCall = this.$.primerocall;
        window.segundoCall = this.$.segundocall;
    }
    static get properties() {
        return {
            imgsrchat: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/upb_button.svg");
                }
            },
            imgsrcall: {
                type: String,
                value: function() {
                    return this.resolveUrl(
                        pathUrl + "/widget/upb/img/upb_button_call.svg"
                    );
                }
            },
            imgsrclose: {
                type: String,
                value: function() {
                    return this.resolveUrl(pathUrl + "/widget/upb/img/close.png");
                }
            }
        };
    }

    visualiza_primero() {
        this.$.primero.style.visibility = "visible";
        this.$.primero.style.display = "block";
        this.$.segundo.style.visibility = "hidden";
        this.$.segundo.style.display = "none";
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
        if (localStorage.getItem(nomlocalStor)) {
            var displayed = this.$.chatWin2.style.display;
            if (displayed == "none") {
                this.$.chatWin2.style.display = "";
                window.FormCall.style.display = "none";
                window.segundoCall.style.visibility = "hidden";
                window.segundoCall.style.display = "none";
                window.primeroCall.style.visibility = "visible";
                window.primeroCall.style.display = "block";
            } else {
                this.$.chatWin2.style.display = "none";
            }
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        } else {
            var displayed = this.$.chatWin.style.display;
            if (displayed == "none") {
                this.$.chatWin.style.display = "";
                window.FormCall.style.display = "none";
                window.segundoCall.style.visibility = "hidden";
                window.segundoCall.style.display = "none";
                window.primeroCall.style.visibility = "visible";
                window.primeroCall.style.display = "block";
            } else {
                this.$.chatWin.style.display = "none";
            }
            window.boxMess.scrollTop = window.boxMess.scrollHeight;
        }
    }
    visualiza_segundo() {
        this.$.segundo.style.visibility = "visible";
        this.$.segundo.style.display = "block";
        this.$.primero.style.visibility = "hidden";
        this.$.primero.style.display = "none";
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
        if (localStorage.getItem(nomlocalStor)) {
            var displayed = this.$.chatWin2.style.display;
            if (displayed == "none") {
                this.$.chatWin2.style.display = "";
                window.FormCall.style.display = "none";
                window.segundoCall.style.visibility = "hidden";
                window.segundoCall.style.display = "none";
                window.primeroCall.style.visibility = "visible";
                window.primeroCall.style.display = "block";
            } else {
                this.$.chatWin2.style.display = "none";
            }
        } else {
            var displayed = this.$.chatWin.style.display;
            if (displayed == "none") {
                this.$.chatWin.style.display = "";
                window.FormCall.style.display = "none";
                window.segundoCall.style.visibility = "hidden";
                window.segundoCall.style.display = "none";
                window.primeroCall.style.visibility = "visible";
                window.primeroCall.style.display = "block";
            } else {
                this.$.chatWin.style.display = "none";
            }
        }
    }
    visualiza_primero_call() {
        this.$.primerocall.style.visibility = "visible";
        this.$.primerocall.style.display = "block";
        this.$.segundocall.style.visibility = "hidden";
        this.$.segundocall.style.display = "none";
        window.boxMess.scrollTop = window.boxMess.scrollHeight;
        var displayed = this.$.chatWinForm.style.display;
        if (displayed == "none") {
            this.$.chatWinForm.style.display = "";
            this.$.chatWin.style.display = "none";
            this.$.chatWin2.style.display = "none";
            window.segundo.style.visibility = "hidden";
            window.segundo.style.display = "none";
            window.primero.style.visibility = "visible";
            window.primero.style.display = "block";
        } else {
            this.$.chatWinForm.style.display = "none";
        }
    }
    visualiza_segundo_call() {
        this.$.segundocall.style.visibility = "visible";
        this.$.segundocall.style.display = "block";
        this.$.primerocall.style.visibility = "hidden";
        this.$.primerocall.style.display = "none";
        var displayed = this.$.chatWinForm.style.display;
        if (displayed == "none") {
            this.$.chatWinForm.style.display = "";
            this.$.chatWin.style.display = "none";
            this.$.chatWin2.style.display = "none";
            window.segundo.style.visibility = "hidden";
            window.segundo.style.display = "none";
            window.primero.style.visibility = "visible";
            window.primero.style.display = "block";
        } else {
            this.$.chatWinForm.style.display = "none";
        }
    }
}

/*
    ********************************************************************************************************************
    Definición de los elementos personalizados (custom-element) en el navegador
    ********************************************************************************************************************
*/
window.customElements.define("web-icon", WebIcon);
window.customElements.define("web-chat", WebChat);
window.customElements.define("web-form", WebForm);
window.customElements.define("web-form-call", WebFormCall);
/*
    ********************************************************************************************************************
    suscripción del cliente al canal new-message del socket, permite la comunicación del cliente con el usuario
    ********************************************************************************************************************
*/
socket.on("new-message", async function(data) {
    var node = document.createElement("div");

    if (data.action != "suggestions") {
        window.setTimeSugg = 3000;
    } else {
        window.setTimeSugg = 0;
    }

    if (data.action == "human") {
        window.setTimeMsg = 0;
    } else if (data.action == "resetChat") {
        localStorage.setItem(nomCountStor, 0);
        window.setTimeMsg = 2200;
    } else {
        window.setTimeMsg = 2200;
    }

    console.log("ESTA ES LA IMAGEN: ", data.image)
    if (data.image) {
        renderLoadingMsg(node)
        setTimeout(() => {
            renderImage(node, data.image)
        })

    }


    if (data.msg) {
        renderLoadingMsg(node);
        setTimeout(function() {
            renderMessageBot(node, data.msg, nowHour());
            axios.post(
                pathUrl + "/widget/upb/tasks/saveMessage", {
                    idUser: window.idUser,
                    message: data.msg,
                    from: "bot"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
        }, window.setTimeMsg);
    } else if (data.sug) {
        setTimeout(function() {
            if (window.nodoSuggestions != undefined) {
                try {
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                } catch (e) {
                    console.error(e);
                }
            }
            window.nodoSuggestions = document.createElement("div");
            window.nodoSuggestions.setAttribute(
                "class",
                "server-response-suggestions"
            );
            window.nodoSuggestions.setAttribute("id", "suggestion");
            window.boxMess.appendChild(window.nodoSuggestions);
            data.sug.forEach(function(e) {
                var node1 = document.createElement("button");
                node1.setAttribute("class", "sug");
                node1.setAttribute("value", "" + e.title + "");
                node1.innerHTML = e.title;
                window.nodoSuggestions.appendChild(node1);
                window.boxMess.scrollTop = window.boxMess.scrollHeight;
                node1.addEventListener("click", function() {
                    node.setAttribute("class", "message message-personal");
                    var messugestion = node1.getAttribute("value");
                    axios.post(
                        pathUrl + "/widget/upb/tasks/saveMessage", {
                            idUser: window.idUser,
                            message: messugestion,
                            from: "user"
                        }, {
                            headers: {
                                "Content-Type": "application/json",
                                authorization: "Bearer " + window.idTokenJWT
                            }
                        }
                    );
                    renderMessageUser(node, messugestion, nowHour());
                    window.boxMess.removeChild(window.nodoSuggestions);
                    window.nodoSuggestions = undefined;
                    socket.emit("new-message", {
                        message: node1.getAttribute("value"),
                        id: JSON.parse(localStorage.getItem(nomlocalStor)).token
                    });
                    window.prvmess.value = "";
                    window.boxMess.scrollTop = window.boxMess.scrollHeight;
                });
            });
        }, window.setTimeSugg);
    }
});

socket.on("estado", function(msg) {
    if (msg == "nuevo") {
        count = parseInt(localStorage.getItem(nomCountStor)) + 1;
        localStorage.removeItem(nomCountStor);
        localStorage.setItem(nomCountStor, count);
    } else {
        socket.emit("Agent", "Chats", {
            socketId: JSON.parse(localStorage.getItem(nomlocalStor)).token,
            id: window.idUser,
            channel: "webchat"
        });
        localStorage.removeItem(nomCountStor);
        localStorage.setItem(nomCountStor, 3);
        socket.emit(
            "Agent-User",
            JSON.parse(localStorage.getItem(nomlocalStor)).token,
            null
        );
    }
});

socket.on("private-message", function(data) {
    var node = document.createElement("div");
    if (data.messageUser != null) {
        if (data.state == null) {
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                null,
                "webchat",
                "tmoReset"
            );
            renderMessageAgent(node, data.messageUser, nowHour());
        } else if (data.state == 1 || data.state == 5) {
            axios.post(
                pathUrl + "/widget/upb/tasks/saveMessageAgent", {
                    idUser: window.idUser,
                    idSocket: JSON.parse(localStorage.getItem(nomIdConStor)),
                    message: data.messageUser,
                    from: "agent"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
            renderMessageAgent(node, data.messageUser, nowHour());
            localStorage.removeItem(nomCountStor);
            localStorage.removeItem(nomIdConStor);
            localStorage.setItem(nomCountStor, 0);
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                window.idConversation,
                "webchat",
                "tmoFin"
            );
        } else if (data.state == 2) {
            axios.post(
                pathUrl + "/widget/upb/tasks/saveMessageAgent", {
                    idUser: window.idUser,
                    idSocket: data.idCon,
                    message: data.messageUser,
                    from: "agent"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
            renderMessageAgent(node, data.messageUser, nowHour());
            localStorage.setItem(nomIdConStor, data.idCon);
            window.idConversation = data.idCon;
            socket.emit(
                "Agent-User",
                JSON.parse(localStorage.getItem(nomlocalStor)).token,
                null,
                null,
                window.idConversation,
                "webchat",
                "tmo"
            );
        } else if (data.state == 4) {
            axios.post(
                pathUrl + "/widget/upb/tasks/saveMessageAgent", {
                    idUser: window.idUser,
                    idSocket: data.idCon,
                    message: data.messageUser,
                    from: "agent"
                }, {
                    headers: {
                        "Content-Type": "application/json",
                        authorization: "Bearer " + window.idTokenJWT
                    }
                }
            );
            renderMessageAgent(node, data.messageUser, nowHour());
        }
    }
});
/*
  Function para renderizado de mensajes
*/
function renderMessageUser(node, msg, hour) {
    node.setAttribute("class", "message message-personal");
    if (/\.(jpe?g|png|gif|bmp)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatarUser'><img src='http://localhost:5000/widget/upb/img/upb_user_chat.svg'></figure>" +
            msg +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    } else if (/\.(undefined|pdf)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatarUser'><img src='http://localhost:5000/widget/upb/img/user_chat.svg'></figure>" +
            `<a href="${msg}" download>
            Archivo
            <img src="http://localhost:5000/widget/upb/img/download.png" width="30" height="30">
        </a>`; +
        "<p class='putTimestampUser'>" + hour + "</p>";
    } else {
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
        let strNew = msg.replace(urlRegex, function(url) {
            return (
                '<a href="' +
                url +
                '" style="color: var(--secondary-color);">' +
                url +
                "</a>"
            );
        });
        node.innerHTML =
            "<figure class='avatarUser'><img src='http://localhost:5000/widget/upb/img/upb_user_chat.svg'></figure>" +
            strNew +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    }
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Mensaje Loading
*/
function renderLoadingMsg(node) {
    node.setAttribute("class", "message new loading");
    node.innerHTML = "<figure class='avatar'><img src='http://localhost:5000/widget/upb/img/upb_bot_chat.svg'></figure><span></span><span></span><span></span><p class='putTimestampBot'>" + nowHour() + "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
  Funcion para rendedizar mensaje del bot
*/
function renderMessageBot(node, msg, hour) {
    var strNew = msg.replace(/\*(\S(.*?\S)?)\*/gm, "<strong>$1</strong>");
    var strNews = strNew.replace(/(?:\r\n|\r|\n)/g, "<br>");
    let strLink = strNews.replace(
        /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm,
        function(url) {
            return '<a target="autoblank" href="' + url + '">' + url + "</a>";
        }
    );
    node.setAttribute("class", "message new");
    node.innerHTML =
        "<figure class='avatar'><img src='http://localhost:5000/widget/upb/img/upb_bot_chat.svg'></figure>" +
        strLink +
        "<p class='putTimestampBot'>" +
        hour +
        "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}
/*
/*
  Funcion para rendedizar mensaje del Agente
*/
function renderMessageAgent(node, msg, hour) {



    var node = document.createElement("div");
    node.setAttribute("class", "message new");
    if (/\.(jpe?g|png|gif|bmp)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatar'><img src='http://localhost:5000/widget/upb/img/upb_bot_chat.svg'></figure>" +
            `<a href="${msg}" target="_blank"><img src="${msg}" style="width:60%;"/><br></a>` +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    } else if (/\.(undefined|pdf)$/i.test(msg)) {
        node.innerHTML =
            "<figure class='avatar'><img src='http://localhost:5000/widget/upb/img/upb_bot_chat.svg'></figure>" +
            `<a href="${msg}" download>
            Archivo
            <img src="http://localhost:5000/widget/upb/img/download.png" width="30" height="30">
        </a>`; +
        "<p class='putTimestampUser'>" + hour + "</p>";
    } else {
        var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
        let strNew = msg.replace(urlRegex, function(url) {
            return '<a href="' + url + '" style="color: #FFF;">' + url + "</a>";
        });
        node.innerHTML =
            "<figure class='avatar'><img src='http://localhost:5000/widget/upb/img/upb_bot_chat.svg'></figure>" +
            strNew +
            "<p class='putTimestampUser'>" +
            hour +
            "</p>";
    }
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;


}

function renderImage(node, img) {
    node.setAttribute("class", "message new loading");
    const imagen = '<img class="img_mostrar" src="' + img + '">'
    node.innerHTML = "<figure class='avatar'><img src='http://localhost:5000/widget/upb/img/bot_chat1.png'></figure>" + imagen + "<p class='putTimestampBot'>" + nowHour() + "</p>";
    window.boxMess.appendChild(node);
    window.boxMess.scrollTop = window.boxMess.scrollHeight;
}

/*
  Funcion para llamada de horario de atención del día
*/
function isHabilDay() {
    return axios
        .post(pathUrl + "/widget/upb/tasks/isDayHabil", { fecha: nowDate() })
        .then(function(data) {
            return data.data[0];
        });
}
/*
Funcion para convertir formato de hora-minutos-segundos hh:mm:ss a segundos
*/
function convertToInt(num) {
    try {
        let arr = num.split(":");
        let hr = parseInt(arr[0]);
        let min = parseInt(arr[1]);
        let seg = parseInt(arr[2]);
        let result = hr * 3600 + min * 60 + seg;
        return result;
    } catch {
        logger.error("Error, no existe array para hacerle split");
    }
}
/*
Funcion para convertir segundos a formato de hora-minutos hh:mm am/pm
*/
function convertToHour(num) {
    try {
        var hours = Math.floor(num / 3600);
        var minutes = Math.floor((num % 3600) / 60);
        if (hours < 10) hours = "0" + hours;
        if (minutes < 10) minutes = "0" + minutes;
        if (hours > 12) {
            hours = hours - 12;
            apm = "pm";
        } else if (hours == 12) {
            apm = "pm";
        } else {
            apm = "am";
        }
        return hours + ":" + minutes + " " + apm;
    } catch {
        logger.error("Error, no existe array para hacerle split");
    }
}
/*
Funcion para obtener formato de año-mes-dia yyyy:mm:dd
*/
function nowDate() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = date.getMonth() + 1;
    if (gg < 10) gg = "0" + gg;
    if (mm < 10) mm = "0" + mm;
    var cur_day = aaaa + "-" + mm + "-" + gg;
    return cur_day;
}
/*
Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
*/
function nowHour() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var apm = " ";
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    /*if (hours > 12) {
  hours = hours - 12;
  apm = "pm";
} else if (hours == 12) {
  apm = "pm";
} else {
  apm = "am";
}*/
    if (seconds < 10) seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds; //+ " " + apm;
}