const fs = require('fs');
const path = require('path');

function arrayToCsvString(arrayOfData) {
    var csvString = "";
    //creamos contenido del archivo
    for (var i = 0; i < arrayOfData.length; i++) {
        //construimos cabecera del csv
        if (i == 0)
            csvString += Object.keys(arrayOfData[i]).join("°") + "\n";
        //resto del contenido
        csvString += Object.keys(arrayOfData[i]).map(function (key) {
            return arrayOfData[i][key];
        }).join("°") + "\n";
    }
    return csvString;
};

function writeFile(csvString) {
    try {
        const fileName = `${nowDate()}:${nowHour()}.csv`
        fs.writeFileSync(path.join(__dirname, "..", "public", "csv-files", `${fileName}`), csvString, 'utf-8');
        console.log("Archivo escrito satisfactoriamente");
        console.log(fileName);
        return fileName;
    } catch (e) {
        console.error("Error escribiendo archivo")
        console.log(e);
    }
}

function makeCsvFile(arrayOfData) {
    const csvString = arrayToCsvString(arrayOfData);
    return writeFile(csvString);
}

/*
  Funcion para obtener formato de año-mes-dia yyyy:mm:dd
*/
function nowDate() {
    var date = new Date();
    var aaaa = date.getFullYear();
    var gg = date.getDate();
    var mm = date.getMonth() + 1;
    if (gg < 10) gg = "0" + gg;
    if (mm < 10) mm = "0" + mm;
    var cur_day = aaaa + "-" + mm + "-" + gg;
    return cur_day;
  }
  /*
    Funcion para obtener formato de hora-minutos-segundos hh:mm:ss
  */
  function nowHour() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    return hours + ":" + minutes + ":" + seconds;
  }

module.exports = makeCsvFile;